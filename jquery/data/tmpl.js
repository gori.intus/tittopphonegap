var _applist = [
	{
		appid:237,
		newApp:"new",		
		category:"app",
		
		playStore:"market://details?id=com.hellotext.hello",
		reviews:"http://www.theverge.com/2014/2/11/5397850/best-new-apps-hello-sms",
		starRating:"4.3",
		licence:"Free",
		downloads:"100,000+",
		version:"2.2",
		
		name:"hello sms",
		img:"http://tiptoptale.com/data/237-hello-sms/main.jpg",
		desc:"Ever since Google clunkily integrated SMS into Hangouts and killed off the standard SMS app, I've been on the hunt for a better text-messaging app. Fortunately, I've found Hello SMS, a straightforward, simple SMS app. It fully replaces the built-in messaging app on your Android phone and feels like something Google itself could have built.",
	},
	
	{
		appid:236,		
		newApp:"new",		
		live:"wallpaper",
		
		developer:"http://doctor-kettlers.com/",
		playStore:"market://details?id=com.afkettler.earth",
		starRating:"4.4",
		licence:"Free",
		downloads:"100,000+",
		version:"2.3",
		
		name:"Earth & Moon in HD Gyro 3D",
		img:"http://tiptoptale.com/data/236-earth-and-moon-lwp/main.jpg",
		desc:"The most realistic interactive 3D Earth Live Wallpaper ever! (trust me ;-)",
		video:"https://www.youtube.com/embed/3-vOJ1jtNZw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:235,
		category:"app",
		
		playStore:"market://details?id=com.kayan.nurav.apkshare",
		starRating:"4.3",
		licence:"Free",
		downloads:"100,000+",
		version:"1.6",
		
		name:"Apk Share & Backup",
		img:"http://tiptoptale.com/data/235-apk-share/main.jpg",
		desc:"Share Installed Apps Apk (.apk file) via Bluetooth, Email etc. Backup Apks to SDCard / External Storage. Works without root.",
	},
	
	{
		appid:234,
		category:"app",
		newApp:"new",
		
		developer:"http://www.clockworkmod.com/",
		playStore:"market://details?id=com.koushikdutta.backup",
		starRating:"4.4",
		licence:"Free",
		downloads:"500,000+",
		version:"4.0",
		
		name:"Helium - App Sync and Backup",
		img:"http://tiptoptale.com/data/234-helium/main.jpg",
		desc:"Helium is the missing app sync and backup solution for Android. Helium does NOT require root. All Android users can use Helium to backup and sync Android applications. Helium lets you backup your apps and data to your SD card or cloud storage.",
		video:"https://www.youtube.com/embed/GVjC7lhaeLs?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:233,
		category:"app",
		newApp:"new",
		
		playStore:"market://details?id=com.abmantis.galaxychargingcurrent.free",
		starRating:"3.8",
		licence:"Free",
		downloads:"100,000+",
		version:"4.0",
		
		name:"Galaxy Charging Current Lite",
		img:"http://tiptoptale.com/data/233-galaxy-charging-current-lite/main.jpg",
		desc:"Check if your device battery is charging correctly and at the maximum speed! With this application you can check the battery charging electrical current of your Samsung Galaxy devices.",
	},
	
	{
		appid:232,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.rayark.com/g/deemo/",
		playStore:"market://details?id=com.rayark.pluto",
		reviews:"http://www.appspy.com/review/7947/deemo",
		starRating:"4.8",
		licence:"Free",
		downloads:"500,000+",
		version:"4.0",
		
		name:"Deemo",
		img:"http://tiptoptale.com/data/232-deemo/main.jpg",
		desc:"It's been a while since gamers fell out of love with rhythm games. This was mostly due to the over-saturation of rock music sims driven by plastic guitars, but also because of lack of creativity in the games themselves. Deemo does its best to address these issues by not only providing feedback as you tap and slide, but also presenting a fantasy through the melding of music and haunting artwork.",
		video:"https://www.youtube.com/embed/0oTNGhMroiM?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:231,
		category:"game",
		newApp:"new",
		
		playStore:"market://details?id=com.fartie.bird.free",
		starRating:"4.1",
		licence:"Free",
		downloads:"10,000+",
		version:"1.6",
		
		name:"Fartie Bird",
		img:"http://tiptoptale.com/data/231-fartie-bird/main.jpg",
		desc:"Another clone of Flappy Bird. Tap to Fart and Flap. Avoid Obstacles and Pipes. Enjoy this awesome Flappy and Fartie adventure. Join our flatulent but brave Flappy but Fartie bird to sort all the pipes and obstacles.",
	},
	
	{
		appid:230,
		category:"game",
		multi:"multi",
		newApp:"new",
		
		top:"developer",
		developer:"http://maxnick.com/",
		playStore:"market://details?id=com.maxnick.dotsout",
		starRating:"3.8",
		licence:"Free",
		downloads:"1,000+",
		version:"2.3",
		
		name:"Dots Out",
		img:"http://tiptoptale.com/data/230-dots-out/main.jpg",
		desc:"One of the most popular board games on the vast territory of Eurasia was an ancient game Cha Pai. Empires revolved, but people continued to play trying to knock the opponent's chips off the board.",
	},
	
	{
		appid:229,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://mobile.yahoo.com/weather/",
		playStore:"market://details?id=com.yahoo.mobile.client.android.weather",
		reviews:"http://www.theverge.com/2013/8/15/4622882/yahoos-beautiful-new-weather-app-comes-to-android",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.3",
		
		name:"Yahoo Weather",
		img:"http://tiptoptale.com/data/229-yahoo-weather/main.jpg",
		desc:"The updated Yahoo Weather for Android, available across the world today, offers the same full bleed design as its iOS counterpart, along with similarly crisp typography. Background images are handled by Flickr, and are specific to each location.",
		video:"https://www.youtube.com/embed/EqBe50bSKaQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:228,
		category:"app",
		
		developer:"http://www.necta.us/",
		playStore:"market://details?id=wsm.wifimousefree",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"WiFi Mouse",
		img:"http://tiptoptale.com/data/228-wifi-mouse/main.jpg",
		desc:"Transform your phone into a wireless mouse, keyboard and trackpad using WiFi Mouse. WiFi Mouse supports speech-to-text as well as multi-finger trackpad gestures. WiFi Mouse enables you to control your PC, MAC or HTPC effortlessly through a local network connection.",
		video:"https://www.youtube.com/embed/ix4QsM44k1Q?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:227,
		category:"app",
		
		top:"developer",
		developer:"https://www.waze.com/",
		playStore:"market://details?id=com.waze",
		reviews:"http://thegadgetpill.com/waze-free-gps-and-social-app-for-iphone-and-android-review-and-features/",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Waze Social GPS Maps & Traffic",
		img:"http://tiptoptale.com/data/227-waze/main.jpg",
		desc:"What's great about Waze, the fact that we have a constantly changing map, which displays live any problem occurring in traffic, fuel prices or road blocks. Such an interactive map has long been awaited for and now the hundred thousand users have turned it into a 'must have' app.",
		video:"https://www.youtube.com/embed/y_7yoEUrVhw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:226,
		category:"game",
		
		top:"developer",
		developer:"http://blog.voxelrush.com/",
		playStore:"market://details?id=com.hyperbees.RafalWilinski.voxelrush",
		starRating:"4.1",
		licence:"Free",
		downloads:"100,000+",
		version:"2.3",
		
		name:"Voxel Rush: Free Racing Game",
		img:"http://tiptoptale.com/data/226-voxel-rush/main.jpg",
		desc:"Superfast, minimalistic and extreme 3D racing game! Accelerometer-controlled, endless and free racing game. Race through minimalistic landscapes. Avoid obstacles and don't crash! Now with multiplayer! Be fast, be aggressive, race like you know no fear! Get it now!",
		video:"https://www.youtube.com/embed/_92VRqyFMaw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:225,
		category:"app",
		newApp:"new",
		
		developer:"http://www.viki.com/",
		playStore:"market://details?id=com.viki.android",
		starRating:"4.3",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.3",
		
		name:"Viki: Watch Free TV & Movies",
		img:"http://tiptoptale.com/data/225-viki/main.jpg",
		desc:"Watch the best TV shows, movies, variety selection of Korean dramas, and other premium content in high quality, translated into more than 150 languages by a community of avid fans. Top titles include the hottest Korean dramas, Taiwanese, Chinese and Filipino dramas, Telenovelas, Japanese dramas and anime, American cartoons, NBC Universal, History Channel, A&E, E! TV shows",
	},
	
	{
		appid:224,
		category:"app",
		
		top:"developer",
		developer:"http://umanoapp.com/",
		playStore:"market://details?id=com.sothree.umano",
		reviews:"http://www.androidtapp.com/umano-news-read-to-you/",
		starRating:"4.5",
		licence:"Free",
		downloads:"500,000+",
		version:"2.2",
		
		name:"Umano: Listen to News Articles",
		img:"http://tiptoptale.com/data/224-umano/main.jpg",
		desc:"Umano: News Read to You is a new app with a massive difference. Instead of you reading the latest stories on it, it reads them to you! Not only that, stories are read in a friendly and 'human' way, unlike the awkward robotic tones we are used to.",
	},
	
	{
		appid:223,
		category:"app",
		
		top:"developer",
		developer:"http://help.tripadvisor.com/",
		playStore:"market://details?id=com.tripadvisor.tripadvisor",
		reviews:"http://www.androidtapp.com/tripadvisor/",
		starRating:"4.5",
		licence:"Free",
		downloads:"50,000,000+",
		
		name:"TripAdvisor Hotels Flights",
		img:"http://tiptoptale.com/data/223-tripadvisor-hotels-flights/main.jpg",
		desc:"TripAdvisor is a detailed and comprehensive travel companion app with lots of information about restaurants, places of interest, hotels, flights and more. The app is the Android portal to the popular and busy website of the same name, and it allows you to access the forums, reviews and guides right on your Android device.",
	},
	
	{
		appid:222,
		category:"game",
		
		developer:"http://skgames.com/",
		playStore:"market://details?id=com.chillingo.totemrunner.android.rowgplay",
		reviews:"http://playboard.me/android/apps/com.skgames.trafficracer",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.3",
		
		name:"Traffic Racer",
		img:"http://tiptoptale.com/data/222-traffic-racer/main.jpg",
		desc:"Have you ever thought about weaving in between traffic, but decided that your life wasn't worth it? Now you can live out your fantasies and see how long you would last before suffering a painful death at the hands of your own impatience.",
		video:"https://www.youtube.com/embed/yDMsCNyudpo?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:221,
		category:"game",
		
		developer:"http://www.chillingo.com/",
		playStore:"market://details?id=com.chillingo.totemrunner.android.rowgplay",
		reviews:"http://www.appszoom.com/android_games/casual/totem-runner_hqave.html",
		starRating:"4.3",
		licence:"Free",
		downloads:"100,000+",
		version:"2.3",
		
		name:"Totem Runner",
		img:"http://tiptoptale.com/data/221-totem-runner/main.jpg",
		desc:"Totem Runner is a yet another ever-running sidescroller, but fortunately it has a) an incredible design we've fallen in love with; and b) an accurate and balanced gameplay that will keep you playing it for hours and days unless you commit uninstall before the first play.",
		video:"https://www.youtube.com/embed/U0Udoj9v8no?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:220,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://www.bitspin.ch/",
		playStore:"market://details?id=ch.bitspin.timely",
		reviews:"http://www.theverge.com/2013/8/19/4636186/timely-is-the-best-looking-alarm-clock-android",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"4.0",
		
		name:"Timely Alarm Clock",
		img:"http://tiptoptale.com/data/220-timely-alarm-clock/main.jpg",
		desc:"Timely's extensive feature set makes it an easy choice for an alarm clock, but its clean design and fun animations make it a joy to use as well. Timely is available for free in the Google Play Store now.",
		video:"https://www.youtube.com/embed/8hYQobl5xL4?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:219,
		category:"app",
		
		top:"developer",
		developer:"http://www.teamviewer.com/hi/index.aspx",
		playStore:"market://details?id=com.teamviewer.teamviewer.market.mobile",
		reviews:"http://www.androidtapp.com/teamviewer-free/",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"TeamViewer for Remote Control",
		img:"http://tiptoptale.com/data/219-teamviewer/main.jpg",
		desc:"Teamviewer Free. Mobile and flexible: remotely access computers from your android device! Easily support friends and family when they have computer problems - no matter where you are. You can also benefit from gaining access to your private home computer to edit documents or use particular software while you are on the road.",
		video:"https://www.youtube.com/embed/yEofo1tWaoc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:218,
		category:"game",
		
		developer:"http://www.clapfootgames.com/",
		playStore:"market://details?id=com.clapfootgames.tankhero",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Tank Hero",
		img:"http://tiptoptale.com/data/218-tank-hero/main.jpg",
		desc:"Tank Hero. Fast paced 3D tank action on your Android phone. Take out your enemies with cannons, heat seekers, and howitzers. Battle against cunning enemies and become the Tank Hero! FEATURES:  OpenGL 3D graphics, Campaign and Survival game modes with 40 levels each, 4 weapons to choose from, 5 types of AI tanks to fight against, Multi-touch and trackball control schemes.",
		video:"https://www.youtube.com/embed/jJIYF7aCtPM?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:217,
		category:"app",
		widget:"widget",
		newApp:"new",
		
		developer:"http://www.androidm8.com/",
		playStore:"market://details?id=com.androidm8.speakerphoneex",
		reviews:"http://playboard.me/android/apps/com.androidm8.speakerphoneex",
		starRating:"4.2",
		licence:"Free",
		downloads:"10,000+",
		version:"2.1",
		
		name:"SpeakerPhone Ex",
		img:"http://tiptoptale.com/data/217-speaker-phone-ex/main.jpg",
		desc:"Your phone turns itself on when you take it out of your pocket, but why do you have to manually answer incoming calls before you bring it up to your ear? SpeakerPhone Ex makes it possible to answer incoming calls just by performing that action.",
	},
	
	{
		appid:216,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://soundcloud.com/mobile",
		playStore:"market://details?id=com.soundcloud.android",
		reviews:"http://www.theverge.com/2013/3/21/4132324/soundcloud-updated-for-ios-and-android-support-for-sets",
		starRating:"4.3",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"SoundCloud - Music & Audio",
		img:"http://tiptoptale.com/data/216-soundcloud/main.jpg",
		desc:"SoundCloud has big ambitions - the company wants to leverage new partnerships with artists to become the leading destination for users to connect with musicians they're interested in. To that end, the company's releasing new iOS and Android apps today that add a few key features as well as a nice visual refresh",
	},
	
	{
		appid:215,
		category:"game",
		
		developer:"http://www.hutchgames.com/",
		playStore:"market://details?id=com.hutchgames.smashcopsheat",
		reviews:"http://www.appszoom.com/android_games/racing/smash-cops-heat_fvwqa.html",
		starRating:"4.3",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.3",
		
		name:"Smash Cops Heat",
		img:"http://tiptoptale.com/data/215-smash-cops-heat/main.jpg",
		desc:"Take part in the action, arrest criminals and chase after cars. Smash Cops Heat is a car action game in which you're an important cop driving a car and trying to arrest hundreds of enemies that have run away.",
		video:"https://www.youtube.com/embed/5sB_uUlXye8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:214,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://crescentmoongames.com/",
		playStore:"market://details?id=com.crescentmoongames.slingshotracing",
		reviews:"http://slingshot-racing.appszoom.com/android",
		starRating:"4.6",
		licence:"$0.99",
		downloads:"50,000+",
		version:"2.2",
		
		name:"Slingshot Racing",
		img:"http://tiptoptale.com/data/214-slingshot-racing/main.jpg",
		desc:"Slingshot Racing is one of the most original racing games on Android. No steering wheel, no engine and no brakes. Just an sledge car, an iced track and a slingshot is what you have to beat the other drivers. SR is an original physics-based racing game which tests your reflexes to the fullest.",
		video:"https://www.youtube.com/embed/wRgvA5yImUM?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:213,
		category:"game",
		newApp:"new",
		
		developer:"http://rexetstudio.weebly.com/",
		playStore:"market://details?id=com.RexetStudio.SlenderTheRoadLite",
		starRating:"4.2",
		licence:"Free",
		downloads:"100,000+",
		version:"2.0",
		
		name:"Slender: The Road Lite",
		img:"http://tiptoptale.com/data/213-slender-the-road-lite/main.jpg",
		desc:"The main character by the name of Tom receives from the son the message on an answering machine. In the message the son asks about the help since someone pursued it but eventually communication breaks and Tom goes on searches of the son. At the hero at himself only phone and a video camera which he took to finish shooting all events.",
		video:"https://www.youtube.com/embed/lM2LpuuScXE?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:212,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.idreams.pl/en/",
		playStore:"market://details?id=pl.idreams.ShootTheZombirds",
		reviews:"http://www.androidtapp.com/shoot-the-zombirds/",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.0",
		
		name:"Shoot The Zombirds",
		img:"http://tiptoptale.com/data/212-shoot-the-zombirds/main.jpg",
		desc:"Shoot the Zombirds is a cute and addictive shooting game available on Android and iOS. Basically, you are Pumpkin Boy and you have to stop the Zombirds from kidnapping the pumpkids. You have a trusty crossbow and a smattering of power-ups and, as the birds pass above you, shoot them down!",
		video:"https://www.youtube.com/embed/nuwX_up0q_w?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:211,
		category:"app",
		newApp:"new",
		
		top:"developer",
		developer:"http://simplenote.com/",
		playStore:"market://details?id=com.automattic.simplenote",
		reviews:"http://www.theverge.com/2013/9/18/4741908/simplenote-reborn-first-great-notes-app-ios-android-mac-web",
		starRating:"4.2",
		licence:"Free",
		downloads:"100,000+",
		version:"4.0",
		
		name:"Simplenote",
		img:"http://tiptoptale.com/data/211-simplenote/main.jpg",
		desc:"Simplenote reborn: the first great notes app is back. The award-winning Simplenote is now available for Android. Simplenote is an easy way to keep notes, lists, ideas and more. Your notes stay in sync with all of your devices for free.",
	},
	
	{
		appid:210,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.headupgames.com/conpresso/_rubric/index.php?rubric=EN+Startseite",
		playStore:"market://details?id=com.headupgames.shinyfree",
		reviews:"http://www.appszoom.com/android_games/casual/shiny-the-firefly-free_gbkfw.html",
		starRating:"4.1",
		licence:"Free",
		downloads:"10,000+",
		version:"2.3",
		
		name:"Shiny The Firefly FREE",
		img:"http://tiptoptale.com/data/210-shiny-the-firefly/main.jpg",
		desc:"Shiny The Firefly is a colorful and engaging game where you're required of looking for Shiny's babies. There will be many enemies attacking you at all times, so you need to be clever and think strategically in order to avoid the mosquitoes, plants, toads, wasps and other garden inhabitants who are eager to annoy you.",
		video:"https://www.youtube.com/embed/bm6Sqs7NPcs?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:209,
		category:"app",
		newApp:"new",
		
		playStore:"market://details?id=mohammad.adib.roundr",
		reviews:"http://www.androidpolice.com/2013/04/16/new-app-roundr-rounds-the-corners-of-your-devices-screen-because-why-not/",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000+",
		version:"2.1",
		
		name:"Roundr - Round Screen Corners",
		img:"http://tiptoptale.com/data/209-roundr-round-screen-corners/main.jpg",
		desc:"RoundR Rounds The Corners Of Your Device's Screen, Because Why Not? Basically, it rounds the corners of your homescreen and all apps. Rounded. Corners. That's it.",
	},
	
	{
		appid:208,
		category:"app",
		newApp:"new",
		
		playStore:"market://details?id=ui.robot.rotate",
		starRating:"4.4",
		licence:"Free",
		downloads:"50,000+",
		version:"2.2",
		
		name:"Rotation Lock Adaptive (Free)",
		img:"http://tiptoptale.com/data/208-rotation-lock-adaptive/main.jpg",
		desc:"If you find automatic screen rotation annoying then this app is for you. It will detect the device's orientation and show a transparent button to let you decide whether the screen should be rotated to landscape or portrait.",
		video:"https://www.youtube.com/embed/b3oC15ilXr8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:207,
		category:"game",
		multi:"multi",
		
		developer:"http://www.roadwarriorgame.com",
		playStore:"market://details?id=com.mobjoy.roadwarrior",
		reviews:"http://www.androidtapp.com/road-warrior/",
		starRating:"4.7",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.3",
		
		name:"Road Warrior: Best Racing Game",
		img:"http://tiptoptale.com/data/207-road-warrior/main.jpg",
		desc:"Road Warrior is a full-throttle racing game for Android. It is similar in genre to the to MotoXtreme-style racers; 2D jumping, flipping and trying to land the right way up.",
		video:"https://www.youtube.com/embed/wfev79Qj1cg?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:206,
		category:"game",
		
		top:"developer",
		developer:"http://www.hexage.net/",
		playStore:"market://details?id=net.hexage.reaper",
		reviews:"http://www.androidpolice.com/2013/09/14/reaper-review-an-rpg-that-truly-understands-mobile-gaming/",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"4.0",
		
		name:"Reaper",
		img:"http://tiptoptale.com/data/206-reaper/main.jpg",
		desc:"This is one of the most enjoyable mobile games I've played. Hexage understands how to do mobile games. You can play Reaper for a few minutes at a time and enjoy every second of it.",
		video:"https://www.youtube.com/embed/eBOuCtG3yno?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:205,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://www.reliancegames.com/",
		playStore:"market://details?id=com.jumpgames.rswrb",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/real-steel-world-robot-boxing_iaqfu.html",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Real Steel World Robot Boxing",
		img:"http://tiptoptale.com/data/205-real-steel-world-robot-boxing/main.jpg",
		desc:"RSWRB, to abbreviate, is a 2D fighting game officially set after the events that took place in Real Steel the movie. In short, it's a pretty simple fighting game without much ado, just a few buttons and fewer combinations, but easy to play and moderately enjoyable both by people not used to games of this kind and those who already know what is to virtually beat people on a screen (ha-dooo-ken!)",
		video:"https://www.youtube.com/embed/dJgN3biH2vg?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:204,
		category:"app",
		newApp:"new",
		
		developer:"http://iamrobj.com/",
		playStore:"market://details?id=robj.readit.tomefree",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000+",
		version:"2.2",
		
		name:"ReadItToMe - Unique Handsfree",
		img:"http://tiptoptale.com/data/204-readittome/main.jpg",
		desc:"ReadItToMe Pro reads out your incoming callers, sms messages and other app notifications, translating txt speak into normal language. In ANY language. It's great for use whilst driving, cycling, running, walking, in the gym or any other time you need to be handsfree.",
		video:"https://www.youtube.com/embed/aYwsSd9Z8L8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:203,
		category:"app",
		widget:"widget",
		
		developer:"http://qrdroid.com/",
		playStore:"market://details?id=la.droid.qr",
		reviews:"http://www.appszoom.com/android_applications/productivity/qr-droid_otfm.html",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.0",
		
		name:"QR Droid&trade;",
		img:"http://tiptoptale.com/data/203-qrdroid/main.jpg",
		desc:"QR Droid allows you to read and create QRs containing URL, contact info, app, phone number and many other. You can do basically two operations with QR Droid: Scan and create QR codes.",
	},
	
	{
		appid:202,
		category:"app",
		newApp:"new",
		
		developer:"https://www.pushbullet.com/",
		playStore:"market://details?id=com.pushbullet.android",
		reviews:"http://www.theverge.com/2013/3/23/4132926/nexus-look-on-any-android-phone",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000+",
		version:"4.0",
		
		name:"Pushbullet",
		img:"http://tiptoptale.com/data/202-pushbullet/main.jpg",
		desc:"PushBullets eases getting links, files, pics and notes on and off your phone. This is one of the most useful apps you can find on the market. What does it do? It pushes links, files, pics, notes and lists between devices. It may sound as something that you can already do with other tools. However, it makes it way easier.",
		video:"https://www.youtube.com/embed/jFLEM46Vltk?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:201,
		category:"app",
		multi:"multi",
		newApp:"new",
		
		playStore:"market://details?id=com.bambuna.podcastaddict",
		starRating:"4.5",
		licence:"Free",
		downloads:"100,000+",
		version:"2.2",
		
		name:"Podcast Addict",
		img:"http://tiptoptale.com/data/201-podcast-addict/main.jpg",
		desc:"With Podcast Addict, manage all your audio & video Podcasts as well as your YouTube channels and News feeds from your Android device.",
	},
	
	{
		appid:200,
		category:"game",
		
		top:"developer",
		developer:"http://www.pocketlegends.com/",
		playStore:"market://details?id=sts.pl",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/pocket-legends_ngqy.html",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Pocket Legends",
		img:"http://tiptoptale.com/data/200-pocket-legends/main.jpg",
		desc:"Pocket Legends is a fantasy-themed MMORPG for Android. It works properly whichever the way getting low ping and no lag. It's better if you play it over WiFi, since it updates always before starting the game, and extra data batches can be heavy.",
		video:"https://www.youtube.com/embed/39UfdSHQ8Hc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:199,
		category:"game",
		
		top:"developer",
		developer:"http://support.popcap.com/agegate.php?site=PopCap.com&dst=http%3A%2F%2Fsupport.popcap.com%2Fmobile%2Fandroid",
		playStore:"market://details?id=com.ea.game.pvz2_row",
		reviews:"http://www.androidpolice.com/2013/10/24/plants-vs-zombies-2-review-this-time-the-zombies-want-your-brains-and-your-money-but-that-might-be-okay/",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.3",
		
		name:"Plants vs. Zombies&trade; 2",
		img:"http://tiptoptale.com/data/199-plants-vs-zombies2/main.jpg",
		desc:"Have you played the original Plants vs. Zombies? If you just shouted 'yes' at the top of your lungs, then you pretty much know how to play Plants vs. Zombies 2. The undead will stagger/fly/ride in from the right, and you have to keep them from reaching the far left of the screen.",
		video:"https://www.youtube.com/embed/wCw3hl29_RY?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:198,
		category:"app",
		
		developer:"http://novalauncher.com/",
		playStore:"market://details?id=com.teslacoilsw.launcher",
		reviews:"http://www.theverge.com/2013/3/23/4132926/nexus-look-on-any-android-phone",
		starRating:"4.7",
		licence:"Free",
		downloads:"5,000,000+",
		version:"4.0",
		
		name:"Nova Launcher",
		img:"http://tiptoptale.com/data/198-nova-launcher/main.jpg",
		desc:"My launcher replacement of choice is Nova Launcher. It's fast, stable, and works well right out of the box. Plus, the free version includes all the features that most users will need, though even more tweaks can be unlocked with the $4.00 Nova Prime upgrade.",
	},
	
	{
		appid:197,
		category:"app",
		
		developer:"https://markushintersteiner.at/",
		playStore:"market://details?id=at.markushi.expensemanager",
		starRating:"4.4",
		licence:"Free",
		downloads:"500,000+",
		version:"2.3",
		
		name:"Expense Manager",
		img:"http://tiptoptale.com/data/197-expense-manager/main.jpg",
		desc:"Manage your expenses directly on your smartphone. Easily keep track of your finances. This app allows you to record your expenses easily. Optionally you can assign a category to your expense in order to get detailed statistics and helpful insights.",
	},
	
	{
		appid:196,
		category:"game",
		
		developer:"http://originatorkids.com/",
		playStore:"market://details?id=com.originatorkids.EndlessAlphabet",
		starRating:"4.6",
		licence:"Free",
		downloads:"50,000+",
		version:"2.3",
		
		top:"developer",
		name:"Endless Alphabet",
		img:"http://tiptoptale.com/data/196-endless-alphabet/main.jpg",
		desc:"Set the stage for reading success with this delightfully interactive educational app. Kids will have a blast learning their ABC's and building vocabulary with the adorable monsters in Endless Alphabet. Each word features an interactive puzzle with talking letters and a short animation illustrating the definition. Before you know it, your child will be using words like gargantuan and cooperate! Try it for free today!",
		video:"https://www.youtube.com/embed/eYl0bLO26Ig?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:195,
		category:"app",
		
		playStore:"market://details?id=com.bazinga.cacheclean",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Cache Cleaner Easy",
		img:"http://tiptoptale.com/data/195-cache-cleaner-easy/main.jpg",
		desc:"Cache Cleaner Easy is an android system tool which can clean useless application cache files, it could save your phone disk space and make your phone speed up.",
	},
	
	{
		appid:194,
		category:"app",
		
		developer:"http://www.renkmobil.com/",
		playStore:"market://details?id=com.hwkrbbt.downloadall",
		starRating:"4.1",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Download All Files",
		img:"http://tiptoptale.com/data/194-download-all-files/main.jpg",
		desc:"This app works like a browser plugin and enables you to download all file types and allows you to save them easily to your SD card. Additionally you will be able to save all your Gmail attachments.",
	},
	
	{
		appid:193,
		category:"game",
		
		playStore:"market://details?id=com.ansangha.drdriving",
		reviews:"http://www.appszoom.com/android_games/racing/dr-driving_gtoxr.html",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Dr. Driving",
		img:"http://tiptoptale.com/data/193-dr-driving/main.jpg",
		desc:"This game we have not to drive flashy sport cars, and neither big trucks or unreal four-wheel drives through impossible hills. You'll drive a familiar car and your goal will be to reach certain goals in a standard city in a rush hour. This means precise maneuvers, keep an average safe speed and do not any foolishness when turning or parking.",
		video:"https://www.youtube.com/embed/yjyUYz2jxZI?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:192,
		category:"app",
		newApp:"new",
		
		developer:"http://zenday-app.com/",
		playStore:"market://details?id=com.mobisysteme.zime",
		reviews:"http://www.androidtapp.com/zime-to-do-and-calendar/",
		starRating:"3.8",
		licence:"Free",
		downloads:"100,000+",
		version:"4.0",
		
		name:"ZenDay: Tasks, To-do, Calendar",
		img:"http://tiptoptale.com/data/192-zenday/main.jpg",
		desc:"ZenDay (formerly Zime) To-do and Calendar is a beautiful and innovative calendar and to-do app that is a practical, elegant and original way to manage your productivity. The key feature of the app is its user interface- a crisp 3D wheel of dates that allows you to conceptualize your week in a new way. You can also add events and tasks in addition to easily skipping forward and backwards.",
		video:"https://www.youtube.com/embed/sOqKW6j3fzY?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:191,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://support.wbgames.com/ics/support/default.asp?deptID=24028&_referrer=",
		playStore:"market://details?id=com.wb.goog.injustice",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/injustice-gods-among-us_ihljo.html",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"4.0",
		
		name:"Injustice: Gods Among Us",
		img:"http://tiptoptale.com/data/191-injustice/main.jpg",
		desc:"One of our oldest demands was that 2D fighting games, mainly heirs to Street Fighter and King of Fighters were not landing on mobile devices and how much we'd like to enjoy old school fighting on a tablet or smartphone. Therefore, here it is Injustice.",
	},
	
	{
		appid:190,
		category:"game",
		
		top:"developer",
		developer:"http://pikpok.com/",
		playStore:"market://details?id=com.pikpok.fkff",
		reviews:"http://www.trustedreviews.com/opinions/10-best-android-apps-february-2011_Page-9",
		starRating:"4.3",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"Flick Kick Football Kickoff",
		img:"http://tiptoptale.com/data/190-flick-kick-football-kickoff/main.jpg",
		desc:"The game really is very simple. You take control of a player who is lined up in front of the goal and you just flick your finger up the screen to take your shot. However, you can add spin to the ball by swiping in a curve and increase the power of your shot by making longer swipes.",
	},
	
	{
		appid:189,
		category:"game",
		newApp:"new",
		multi:"multi",
		
		playStore:"market://details?id=com.byril.battleship2",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/battleship_foqok.html",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Sea Battle Online (aka Battleship 2)",
		img:"http://tiptoptale.com/data/189-battleship2/main.jpg",
		desc:"Protect your ships and destroy the others. Battleship is a classic board game with new and improved features that are added in this version for Android. The graphics are doodle-like, perfectly appropriate for the game and appealing for both kids and adults who want to spend an entertaining time thinking strategically and exercising their mind and logic skills.",
		video:"https://www.youtube.com/embed/MGh7IBWc694?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:188,
		category:"app",
		
		developer:"http://line.me/en/",
		playStore:"market://details?id=jp.naver.linecamera.android",
		reviews:"http://line-camera.en.softonic.com/android",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"LINE camera",
		img:"http://tiptoptale.com/data/188-line-camera/main.jpg",
		desc:"Line Camera is a camera app that lets you customize your images with text, stamps, and effects. The point of Line Camera is to take pictures and then add filter effects to the image. After applying the filter, you can add different cartoon stamps to the image. This can either be adding various characters or even adding glasses to the pictures you have.",
		video:"https://www.youtube.com/embed/7u0K_O-zUqQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:187,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.herocraft.com/",
		playStore:"market://details?id=com.herocraft.game.free.icerage",
		starRating:"4.2",
		licence:"Free",
		downloads:"500,000+",
		version:"2.3",
		
		name:"Ice Rage Free",
		img:"http://tiptoptale.com/data/187-ice-rage-free/main.jpg",
		desc:"#1 Sports Game on smartphone in the US and 70 other countries. Over 2 million players! Ice Rage brings ice hockey into the 21st century, whilst paying tribute to 1984's Hat Trick. Players face off across a ice rink, in a hockey duel - one player and one keeper one each side.",
		video:"https://www.youtube.com/embed/Oihv6L5ThTc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:186,
		category:"game",
		
		developer:"http://kiloo.com/games/frisbee-forever",
		playStore:"market://details?id=com.kiloo.frisbeeforever",
		reviews:"http://www.androidtapp.com/frisbee-forever/",
		starRating:"4.3",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Frisbee(R) Forever",
		img:"http://tiptoptale.com/data/186-frisbee-forever/main.jpg",
		desc:"Frisbee Forever is the official app for Frisbee lovers everywhere. The game features exciting gameplay, gorgeous 3D environments, sharp HD graphics, a huge amount of levels and items to collect. The game is also wonderfully immersive and beautiful to play.",
		video:"https://www.youtube.com/embed/Y7ishFcc3XU?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:185,
		category:"game",
		
		playStore:"market://details?id=com.fullfat.android.flickgolffree",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"Flick Golf! Free",
		img:"http://tiptoptale.com/data/185-flick-golf/main.jpg",
		desc:"Flick Golf is back and FREE TO PLAY, featuring stunning new graphics, faster gameplay and more competition. No clubs. No rules. Just flick, spin and curve your shots to try and sink that perfect hole in one. Simply more addictive than ever!",
	},
	
	{
		appid:184,
		category:"game",
		
		developer:"http://iphone.ezone.com/",
		playStore:"market://details?id=com.ezone.Diversion",
		reviews:"http://www.androidtapp.com/diversion/",
		starRating:"4.7",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.0",
		
		name:"Diversion",
		img:"http://tiptoptale.com/data/184-diversion/main.jpg",
		desc:"Diversion is a new, fun-packed 3D platformer that is as addictive as it is beautiful. Guide your running character to the end of the level, collect stars and gems and avoid the spikes and lasers. Requiring a high-end device to pull off the lustrous 3D environments, the game has a huge 100 levels across 3 different worlds.",
		video:"https://www.youtube.com/embed/HgeSFVFYyWQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:183,
		category:"game",
		newApp:"new",
		
		playStore:"market://details?id=com.droidstudio.game.devil2",
		reviews:"http://www.appszoom.com/android_games/sports_games/devil-ninja-2_bhvkv.html",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.0",
		
		name:"Devil Ninja 2",
		img:"http://tiptoptale.com/data/183-devil-ninja2/main.jpg",
		desc:"Kill the enemies, jump over cliffs... same old game, lots of fun Devil Ninja2 is an Android game where you need to battle against the monsters and kill the boss. On the way, you need to jump over the cliffs and long press the fire button to charge the energy and kill the enemies.",
	},
	
	{
		appid:182,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.glu.com/",
		playStore:"market://details?id=com.glu.deerhunt2",
		reviews:"http://www.gamezebo.com/games/deer-hunter-2014/review",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"DEER HUNTER 2014",
		img:"http://tiptoptale.com/data/182-deer-hunter/main.jpg",
		desc:"Deer Hunter 2014 is basically a collection of simple hunts for various types of wild animals. It's a bit less involved than most other hunting sims of course - most likely to cater to the pick-up-and-play mobile audience. Instead of focusing on the chase, it's all about the kill shot. Where the animal is hit (head, heart, lungs, etc) can make all the difference.",
		video:"https://www.youtube.com/embed/ANXLX-gpZ4M?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:181,
		category:"app",
		newApp:"new",
		
		top:"developer",
		developer:"http://cloud.tv/apps/dayframe/",
		playStore:"market://details?id=cloudtv.dayframe",
		reviews:"http://www.androidtapp.com/dayframe/",
		starRating:"4.0",
		licence:"Free",
		downloads:"100,000+",
		version:"2.3",
		
		name:"Dayframe All-in-One Slideshow",
		img:"http://tiptoptale.com/data/181-dayframe/main.jpg",
		desc:"Dayframe is a neat application that turns your Android device into a digital photo frame. You can use it to display family photos of great photography from around the world. Images are incredibly crisp and the display is customisable. Perhaps you've got a tablet you want to make better use of or want your phone to do more than just sit there and charge.",
		video:"https://www.youtube.com/embed/KMXKvsddG1A?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:180,
		category:"app",
		newApp:"new",
		
		playStore:"market://details?id=org.zezi.dq",
		starRating:"4.2",
		licence:"Free",
		downloads:"10,000+",
		version:"4.2",
		
		name:"Daydream Quotes",
		img:"http://tiptoptale.com/data/180-daydream-quotes/main.jpg",
		desc:"Daydream Quotes is a screensaver that simply displays popular quotes. Daydream lets your Android device display useful and delightful information when idle or docked. Show off your photo albums, get the latest news from Google Currents, and more.",
	},
	
	{
		appid:179,
		category:"app",
		newApp:"new",
		widget:"widget",
		
		developer:"http://code.google.com/p/dashclock/",
		playStore:"market://details?id=net.nurik.roman.dashclock",
		reviews:"http://www.theverge.com/2013/3/13/4014544/best-new-apps-dashclock-widget-android",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"4.2",
		
		name:"DashClock Widget",
		img:"http://tiptoptale.com/data/179-dashclock-widget/main.jpg",
		desc:"DashClock Widget is a new widget for Android 4.2 devices that can be placed on a user's home screen or lock screen. Developed by a Google employee, DashClock provides quick, glance-able information on your device without requiring you to even unlock it. You can see current weather, numbers of unread emails and text messages, upcoming appointments, and more.",
	},
	
	{
		appid:178,
		category:"game",
		newApp:"new",
		
		developer:"http://www.rebeltwins.com/",
		playStore:"market://details?id=com.rebeltwins.daddywasathief",
		reviews:"http://www.androidtapp.com/daddy-was-a-thief/",
		starRating:"4.4",
		licence:"Free",
		downloads:"500,000+",
		version:"2.3",
		
		name:"Daddy Was A Thief",
		img:"http://tiptoptale.com/data/178-daddy-was-a-theif/main.jpg",
		desc:"Daddy Was a Thief is a never ending platform game where you have to help a rotund robber escape the police. He begins at the top of a multi-story complex and has to smash his way down through the ceiling. Collect power-ups, create explosions, collect coins and avoid the feds! It's a fun game with rich cartoon visuals and immersive sound and music.",
		video:"https://www.youtube.com/embed/PVzM8GaTBaI?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:177,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.rayark.com/g/cytus/",
		playStore:"market://details?id=com.rayark.Cytus.full",
		reviews:"http://www.appszoom.com/android_applications/photography/cymera-camera-photo-editor_cdqvu.html",
		starRating:"4.8",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Cytus",
		img:"http://tiptoptale.com/data/177-cytus/main.jpg",
		desc:"Welcome to the Musical World of Cytus. LET'S EXPERIENCE MUSIC 'N ART, BEAT 'N REBOUND! Check the screenshots and you will see the most AWESOME mobile music game ever!",
		video:"https://www.youtube.com/embed/n1CbxvguQ1g?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:176,
		category:"app",
		
		developer:"http://cymera.cyworld.com/cymera/index.asp",
		playStore:"market://details?id=com.cyworld.camera",
		reviews:"http://www.appszoom.com/android_applications/photography/cymera-camera-photo-editor_cdqvu.html",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Cymera - Camera & Photo Editor",
		img:"http://tiptoptale.com/data/176-cymera/main.jpg",
		desc:"Cymera is one of the most complete Android photo app. Let's start by its added-value features: besides ease of use and smoothness, the jewel in the crown of Cymera is its facial recognition technology. What's more, it includes lots of filters, light effects, borders and even stickers. Focus screen is divided in a 9-cells grid for a more accurate shoot.",
		video:"https://www.youtube.com/embed/byK1dKTwKPY?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:175,
		category:"game",
		newApp:"new",
		
		developer:"http://www.about-fun.com/home",
		playStore:"market://details?id=com.aboutfun.cutandhack",
		starRating:"4.1",
		licence:"Free",
		downloads:"5,000+",
		version:"2.3",
		
		name:"Cut and Hack",
		img:"http://tiptoptale.com/data/175-cut-and-hack/main.jpg",
		desc:"The worlds first choice-driven abstract hacking simulator. For free! You are a hacker! Be fast and breach as many systems as possible within the time limit. Stay precise with your cutting to hack more data and dominate the leaderboards. As it's 'blitz' style game, you have to rush and slice all the geometry shapes with your perfectly drawn lines.",
		video:"https://www.youtube.com/embed/cCaNAUqbkho?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:174,
		category:"game",
		
		developer:"http://www.spawnstudios.com/",
		playStore:"market://details?id=com.spawnstudios.CrazyBikers2",
		starRating:"4.3",
		licence:"Free",
		downloads:"100,000+",
		version:"2.2",
		
		name:"Crazy Bikers 2 Free",
		img:"http://tiptoptale.com/data/174-crazy-bikers-2/main.jpg",
		desc:"Crazy Bikers 2 is a unique stunt bike racing game, full of action. You have to race faster than anyone else while doing stunts. If you like stunt bike racing games or dirt bike racing games then Crazy Bikers 2 is for you. You will ride on sand, dirt or ice from Antartica to the hot deserts of Africa. Backflips, wheelies, jumps over huge cliffs, it's all in there!",
		video:"https://www.youtube.com/embed/VFH2qVPUiI4?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:173,
		category:"game",
		
		developer:"http://silvertreemedia.com/",
		playStore:"market://details?id=com.silvertree.cordy2",
		reviews:"http://www.androidtapp.com/cordy/",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Cordy 2",
		img:"http://tiptoptale.com/data/173-cordy2/main.jpg",
		desc:"Cordy is a little robot with a big job - to power up his world! Help him run, jump, push, pull, lift, throw, and swing in this beautiful 3D puzzle platform game. Download now, and get ready to harness the power of pure fun! Cordy includes 4 FREE levels! You can buy more content for real money with In-App Billing. This feature can be turned off in the game's Settings.",
		video:"https://www.youtube.com/embed/Viy1LWEKfnU?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:172,
		category:"game",
		
		top:"developer",
		developer:"http://www.glu.com/support",
		playStore:"market://details?id=com.glu.android.ck",
		reviews:"http://www.androidtapp.com/contract-killer-2/",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.0",
		
		name:"Contract Killer",
		img:"http://tiptoptale.com/data/172-contract-killer/main.jpg",
		desc:"Contract Killer 2 is a freemium sequel from Glu Mobile that sees you in the shoes of Jack Griffin, the ultimate contract killer/assassin, call him what you will. In a game that literally drips in testosterone, macho Jack has to complete a series of increasingly difficult kills and other tasks in a variety of environments.",
		video:"https://www.youtube.com/embed/v5V_KcgSQyM?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:171,
		category:"game",
		
		developer:"http://www.aifactory.co.uk/",
		playStore:"market://details?id=uk.co.aifactory.chessfree",
		reviews:"http://www.androidtapp.com/chess-free/",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"1.5",
		
		name:"Chess Free",
		img:"http://tiptoptale.com/data/171-chess-free/main.jpg",
		desc:"Chess Free is Google Play top developer, AI Factory's, rendition of the age old classic board game of Chess. You play against a computer AI, with difficulty levels from novice to expert or you can pass and play with friends on the same device. It's incredibly smooth, that's why it's one of the top ranked Chess games for Android!",
	},
	
	{
		appid:170,
		category:"game",
		multi:"multi",
		
		playStore:"market://details?id=com.zagmoid.carrom3d",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Carrom 3D",
		img:"http://tiptoptale.com/data/170-carrom3d/main.jpg",
		desc:"Carrom 3D will give you the experience of playing with a real carrom board on your android phone and android tablet. You can play with the automatic machine (with difficulty levels beginner, intermediate, or expert), or with a friend using the same phone or using another android phone/tablet as a network game through WIFI or Bluetooth.",
		video:"https://www.youtube.com/embed/q1mjz_iiTTU?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:169,
		category:"game",
		
		top:"developer",
		developer:"http://www.idreams.pl/en/",
		playStore:"market://details?id=pl.idreams.CanKnockdown3",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/can-knockdown-3_ggztl.html",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"4.0",
		
		name:"Can Knockdown 3",
		img:"http://tiptoptale.com/data/169-can-knockdown3/main.jpg",
		desc:"A physics puzzle game inspired in milk can fair games. What it started as a blatant casual game has been evolving into a more sophisticate and enjoyable for the mind physics game. Likewise, graphics and animations are more elaborate than you would expect and gameplay has been enhanced to a point ever undreamed of in the tossing genre.",
		video:"https://www.youtube.com/embed/-4dtHbIaeHc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},

	{
		appid:168,
		category:"app",
		
		developer:"http://www.aldiko.com/",
		playStore:"market://details?id=com.aldiko.android&feature=nav_result",
		reviews:"http://www.androidtapp.com/aldiko-book-reader/",
		starRating:"4.3",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Aldiko Book Reader",
		img:"http://tiptoptale.com/data/168-aldiko/main.jpg",
		desc:"Aldiko Book Reader allows you to download and read thousands of books right on your Android phone. Anywhere. Anytime. Browse extensive catalogs of ebooks (most of them FREE) and download them from inside the app. Comfortable and highly customizable reading experience (font/background color, font type, margin, night mode, etc.).",
	},
	
	{
		appid:167,
		category:"app",
		
		top:"developer",
		developer:"http://www.barnesandnoble.com/u/nook-for-android/379003595",
		playStore:"market://details?id=bn.ereader&hl=en",
		reviews:"http://www.androidtapp.com/nook-for-android-tablet-edition/",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"NOOK - Read Books & Magazines",
		img:"http://tiptoptale.com/data/167-nook/main.jpg",
		desc:"Now you can access your favorite NOOK Books, magazines and newspapers, right from your Android smartphone or tablet.* Shop over 2 million titles including everything from hot new releases to bestsellers. Plus, get instant access to hundreds of FREE NOOK Books, and sample any NOOK Book for FREE.",
	},
	
	{
		appid:166,
		category:"app",
		
		developer:"https://www.camera360.com/",
		playStore:"market://details?id=vStudio.Android.Camera360",
		reviews:"http://www.androidtapp.com/camera360-ultimate/",
		starRating:"4.5",
		licence:"Free",
		downloads:"50,000,000+",
		version:"2.3",
		
		name:"Camera360 Ultimate",
		img:"http://tiptoptale.com/data/166-camera360-ultimate/main.jpg",
		desc:"Camera360 Ultimate is a cool camera and photo editor for Android. While it has been around for a while it has recently received a comprehensive update that has improved UI, options and functionality across the entire application. If you like adding filters, manipulating and sharing images- this is an awesome app to check out.",
		video:"https://www.youtube.com/embed/iIwQ17-_f5Y?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:165,
		category:"game",
		
		developer:"http://www.kyworks.com/",
		playStore:"market://details?id=com.kyworks.buttonsandscissors.inapp",
		reviews:"http://www.androidtapp.com/buttons-and-scissors/",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Buttons and Scissors",
		img:"http://tiptoptale.com/data/165-buttons-and-scissors/main.jpg",
		desc:"On the face of it, Buttons and Scissors might seem like the twee, cutesy little puzzle game that you might get your Grandmother to play, but beneath the haberdashery surface lies something genuinely challenging. Easy to play, tough to complete, it's an excellent little logic game that will have you addicted in no time.",
	},
	
	{
		appid:164,
		category:"game",
		
		top:"developer",
		playStore:"market://details?id=com.midasplayer.apps.bubblewitch",
		reviews:"http://www.appszoom.com/android_games/casual/bubble-witch-saga_ddrui.html",
		starRating:"4.3",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Bubble Witch Saga",
		img:"http://tiptoptale.com/data/164-bubble-witch-saga/main.jpg",
		desc:"Match the potions together and save the realm. Bubble Witch Saga is a casual puzzle game with a powerful storyline. This time, the witches need to get rid of the dark spirits that are attacking their country, so you'll need to travel to the realm and complete each challenge so as to free the land.",
		video:"https://www.youtube.com/embed/VYAm2F88M8E?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:163,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.glu.com/",
		playStore:"market://details?id=com.glu.bombshells",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/bombshells-hells-belles_ctqes.html",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"BOMBSHELLS: HELL'S BELLES",
		img:"http://tiptoptale.com/data/163-bombshells-hells-belles/main.jpg",
		desc:"BOMBSHELLS: HELL'S BELLES is a fast-paced aerial combat game. The world is at high-tech war and all hope is on the aerial battalion. Best pilots here are women, if you don't believe it, test their skills. You can choose among three women, as sexy as dangerous.",
		video:"https://www.youtube.com/embed/xKNCCv2lG24?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:162,
		category:"app",
		newApp:"new",
		
		developer:"http://www.handy-apps.com/main/EasyMoney.aspx",
		playStore:"market://details?id=com.handyapps.billsreminder",
		starRating:"4.4",
		licence:"Free",
		downloads:"100,000+",
		
		name:"Bills Reminder",
		img:"http://tiptoptale.com/data/162-bills-reminder/main.jpg",
		desc:"Keep getting those nasty late payment fees? Download this mobile bill organizer into your Android smartphone now and get daily reminders to pay your bills on time every time! No more easy money for the banks!",
	},
	
	{
		appid:161,
		category:"game",
		newApp:"new",
		
		developer:"http://www.chundos.com/",
		playStore:"market://details?id=com.beast.beat.lite",
		starRating:"4.3",
		licence:"Free",
		downloads:"100,000+",
		version:"2.3",
		
		name:"Beat the Beast Lite",
		img:"http://tiptoptale.com/data/161-beat-the-beast-lite/main.jpg",
		desc:"Beat the Beast is 'Strategy & Action' game with beautiful 3D environments, HD graphics and unique 360 degrees gameplay that will bring you a completely new experience in video games.",
		video:"https://www.youtube.com/embed/Hi2fI_Hh5Jo?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:160,
		category:"game",
		newApp:"new",
		multi:"multi",
		
		top:"developer",
		developer:"http://us.gamevil.com/",
		reviews:"http://www.appszoom.com/android_games/sports_games/_deabm.html",
		playStore:"market://details?id=com.gamevil.bb2013.global",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"Baseball Superstars&reg; 2013",
		img:"http://tiptoptale.com/data/160-baseball-superstars/main.jpg",
		desc:"Baseball Superstars is the most successful Gamevil's series, and it gets millions of downloads every year. This 2013 edition won't be different at all, and it somehow upgrades its predecessor.",
		video:"https://www.youtube.com/embed/YcFAgKI-bSU?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=1",
	},
	
	{
		appid:159,
		category:"app",
		newApp:"new",
		
		developer:"http://badoo.com/",
		reviews:"http://www.appszoom.com/android_applications/social/badoo-meet-new-people_bkhac.html",
		playStore:"market://details?id=com.badoo.mobile",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"Badoo - Meet New People",
		img:"http://tiptoptale.com/data/159-badoo/main.jpg",
		desc:"Badoo is a well known contact website where people upload their photos and interests in order to meet new acquaintances, friends, or God knows who, a new love. It's a straightforward site, without rude photos or unpolite comments, at least publicly. It's very easy and fast to use.",
	},
	
	{
		appid:158,
		category:"game",
		multi:"multi",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.frogmindgames.com/",
		reviews:"http://www.androidtapp.com/badland/",
		playStore:"market://details?id=com.frogmind.badland",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"BADLAND",
		img:"http://tiptoptale.com/data/158-bad-land/main.jpg",
		desc:"BADLAND is a cunning and award winning puzzle game which sees you adventuring through a scary fairy tale land of physics-based dangers. Guide the little forest dweller through the various obstacles and strategically traverse the wild and wonderful environment. The game is utterly, utterly delicious to look at- with silhouetted figures before richly decorated backdrop.",
		video:"https://www.youtube.com/embed/FjysXld6GvI?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},

	{
		appid:157,
		category:"game",
		
		top:"developer",
		developer:"http://www.rovio.com/",
		reviews:"http://www.androidpolice.com/2012/09/27/game-review-rovios-bad-piggies-brings-out-the-engineer-in-all-of-us/",
		playStore:"market://details?id=com.rovio.BadPiggies",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Bad Piggies",		
		img:"http://tiptoptale.com/data/157-bad-piggies/main.jpg",
		desc:"Create the ultimate flying/crawling/rolling/spinning/crashing device and pilot the pigs safely to the eggs!",
		video:"https://www.youtube.com/embed/YsCpDaSooWA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:156,
		category:"app",
		
		top:"developer",
		developer:"http://www.smule.com/",
		playStore:"market://details?id=com.smule.autorap",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"AutoRap by Smule",
		img:"http://tiptoptale.com/data/156-autorap/main.jpg",
		desc:"You simply speak into your phone, and the app chops your voice and buries it in a whole mess of autotuney goodness - engadget.",
		video:"https://www.youtube.com/embed/k1DgNfz1g_s?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:155,
		category:"app",
		
		developer:"http://www.audible.com/",
		reviews:"http://www.engadget.com/2013/07/12/audible-for-android-update/",
		playStore:"market://details?id=com.audible.application",
		starRating:"4.2",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"Audible for Android",
		img:"http://tiptoptale.com/data/155-audible-for-android/main.jpg",
		desc:"Not enough time for all the books you want to enjoy? Now you can turn on and listen to a good book even when you can't pick one up-on your way to work, at the gym, while folding laundry, or any other time your eyes and hands are busy.",
		video:"https://www.youtube.com/embed/0_FSr1g3k7E?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:154,
		category:"app",
		widget:"widget",
		
		developer:"http://infolife.mobi/",
		reviews:"http://www.appszoom.com/android_applications/tools/app-cache-cleaner-1tap-clean_bbymo.html",
		playStore:"market://details?id=mobi.infolife.cache",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		
		name:"App Cache Cleaner - 1Tap Clean",
		img:"http://tiptoptale.com/data/154-app-cache-cleaner/main.jpg",
		desc:"App Cache Clear clears app cache files to free your device memory. Sometimes we don't realize that a slice of our used memory is due to app cache we generate every time we launch an app. When you're running out of memory you start to uninstall apps but there's another solution to clear some used memory: App Cache Cleaner.",
	},
	
	{
		appid:153,
		category:"game",
		
		developer:"http://devexpert.net/androidweather/",
		reviews:"http://www.appszoom.com/android_games/racing/angry-gran-run-running-game_cxaxi.html",
		playStore:"market://details?id=com.aceviral.angrygranrun",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Angry Gran Run - Running Game",
		img:"http://tiptoptale.com/data/153-angry-gran-run/main.jpg",
		desc:"Press and release to make her beat everyone. Angry Gran is a popular game for Android that has a particular main character: the grandma. She doesn't have a good reputation around town and keeps beating people up so as to get money from them.",
	},
	
	{
		appid:152,
		category:"app",
		widget:"widget",
		
		developer:"http://devexpert.net/androidweather/",
		reviews:"http://www.appbrain.com/app/android-weather-clock-widget/com.devexpert.weather",
		playStore:"market://details?id=com.devexpert.weather",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Android Weather & Clock Widget",
		img:"http://tiptoptale.com/data/152-android-weather-and-clock/main.jpg",
		desc:"Android Weather, the quick and accurate weather app, provides detailed weather for all cities worldwide, it searches your address and locates cities quickly, and provides the current temperature, current weather condition, humidity and wind speed and direction, in addition, five, ten days and hourly weather forecast.",
		video:"https://www.youtube.com/embed/WCJG14A1FuQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:151,
		category:"app",
		newApp:"new",
		live:"wallpaper",
		
		playStore:"market://details?id=org.greenbot.technologies.android.jellybean.wallpapers",
		starRating:"4.7",
		licence:"Free",
		downloads:"10,000+",
		version:"4.0",
		
		name:"Android 4.3 Wallpapers - HD",
		img:"http://tiptoptale.com/data/151-android4.3-wallpapers/main.jpg",
		desc:"Android 4.3 Wallpapers is a simple wallpaper app that allows you to change your background wallpaper on your Android device to one of the new Nexus 7 2013 and/or Android 4.3 Wallpapers.",
	},

	
	{
		appid:150,
		category:"game",
		
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/aliens-invasion_bdotx.html",
		playStore:"market://details?id=com.androidgame.Aliens",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000+",
		version:"2.1",
		
		name:"Aliens Invasion",
		img:"http://tiptoptale.com/data/150-aliens-invasion/main.jpg",
		desc:"Aliens Invasion is an action shooting game in which you have to kill aliens. You embody a cowboy whose aim is to end up with the Alien's invasion by killing them all.",
	},
	
	{
		appid:149,
		category:"game",
		
		reviews:"http://www.appszoom.com/android_games/casual/air-control-lite_ekzd.html",
		playStore:"market://details?id=dk.logisoft.aircontrol",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"1.6",
		
		name:"Air Control Lite",
		img:"http://tiptoptale.com/data/149-air-control-lite/main.jpg",
		desc:"Air Control is a really addictive casual game based on air traffic management. Its performance is quite easy: you have to put some order in the air traffic by controlling the direction and the timing the airplanes takes to land.",
		video:"https://www.youtube.com/embed/eXyT6i1vQ8U?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:148,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://www.accuweather.com",
		reviews:"http://www.appszoom.com/android_applications/weather/accuweather_txo.html",
		playStore:"market://details?id=com.accuweather.android",
		starRating:"4.3",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"AccuWeather",
		img:"http://tiptoptale.com/data/148-accuweather/main.jpg",
		desc:"AccuWeather is the official app of the well-known weather forecast service. An app offering a service that is already backed by a great agency with a web service is important. Actually, the quality of the information displayed, even the way that info is displayed meets high standards.",
	},
	
	{
		appid:147,
		category:"game",
		newApp:"new",
		
		playStore:"market://details?id=com.viacom18.colors24THEGAME",
		starRating:"4.3",
		licence:"Free",
		downloads:"100,000+",
		version:"2.0",
		
		name:"24 THE GAME",
		img:"http://tiptoptale.com/data/147-24-the-game/main.jpg",
		desc:"24 THE GAME allows you to live the life of Anil Kapoor(a famous bollywood actor), chief of the Anti-Terrorist Unit of India, while he thwarts an attack by the terrorists and provides an opportunity to take the terrorists head on in a battle of epic proportions.",
	},
	
	{
		appid:146,
		category:"app",
		newApp:"new",
		
		developer:"http://1secondeveryday.com/",
		reviews:"http://dottech.org/121945/android-review-1-second-everyday-app/",
		playStore:"market://details?id=co.touchlab.android.onesecondeveryday",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000+",
		version:"4.0",
		
		name:"1 Second Everyday",
		img:"http://tiptoptale.com/data/146-1-second-everyday/main.jpg",
		desc:"1 Second Everyday is a unique app for Android devices running Android 4.0 or higher. It's not unique in the sense that it offers something completely new and unheard of, it's unique in that it encourages you to do something a little different. With it, you record a one second video clip every day.",
		video:"https://www.youtube.com/embed/1klThsRmX8c?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:145,
		category:"app",
		newApp:"new",
		
		developer:"http://wisesharksoftware.com/",
		playStore:"market://details?id=com.onemanwithstereo",
		starRating:"4.0",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"3D Camera",
		img:"http://tiptoptale.com/data/145-3dcamera/main.jpg",
		desc:"Create 3D images by using standard Android camera. You don't need any additional equipment to create awesome 3D stereo photos. Turn your Android device to stereo camera. 3D glasses are not required, but you can purchase them in Ebay.",
	},
	
	{
		appid:144,
		category:"app",
		
		developer:"http://www.futuremark.com/benchmarks/3dmark",
		reviews:"http://www.appszoom.com/android_applications/tools/3dmark-the-gamers-benchmark_gagvi.html",
		playStore:"market://details?id=com.futuremark.dmandroid.application",
		starRating:"4.2",
		licence:"Free",
		downloads:"100,000+",
		version:"3.1",
		
		name:"3DMark - The Gamer's Benchmark",
		img:"http://tiptoptale.com/data/144-3dmark/main.jpg",
		desc:"3DMark is one of the most popular device's performance test. Benchmark apps run multiple performance test in order to give a score to the device where it is run and compare it with other devices.",
		video:"https://www.youtube.com/embed/zAImZ_Jzk1s?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},

	{
		appid:143,
		category:"app",
		
		developer:"http://zdevs.ru/en/",
		playStore:"market://details?id=ru.zdevs.zarchiver",
		starRating:"4.8",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.0",
		
		name:"ZArchiver",
		img:"http://tiptoptale.com/data/143-zarchiver/main.jpg",
		desc:"ZArchiver - program to manage archives (archive manager). The program has a simple and functional interface. ZArchiver lets you create 7z (7zip), zip, bzip2 (bz2), gzip (gz), XZ, tar... archive types.",
	},
	
	{
		appid:142,
		category:"app",
		
		top:"developer",
		developer:"http://help.tunein.com/customer/portal/topics/39095-android/articles",
		reviews:"http://www.appszoom.com/android_applications/music_and_audio/tunein-radio_kjig.html",
		playStore:"market://details?id=tunein.player",
		starRating:"4.5",
		licence:"Free",
		downloads:"50,000,000+",
		version:"2.3",
		
		name:"TuneIn Radio",
		img:"http://tiptoptale.com/data/142-tune-in-radio/main.jpg",
		desc:"TuneIn is a complete widget that detects where you are and locates the nearest and most popular radio stations around. If you get bored of your favorite station, it suggests other similar stations. You can list your most listened to stations, look for international ones, podcasts or even themed stations.",
	},
	
	{
		appid:141,
		category:"app",
		
		developer:"http://www.sonymobile.com/in/",
		reviews:"http://www.androidtapp.com/trackid/",
		playStore:"market://details?id=com.sonyericsson.trackid",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"TrackID&trade;",
		img:"http://tiptoptale.com/data/141-track-id/main.jpg",
		desc:"TrackID is a music identification service from none other than Sony. While there are quite a few similar apps already in the Google Play Store such as Shazam and SoundHound. However, it's arguably still worth checking out the offering from Sony seeing as they invented the Walkman!",
	},
	
	{
		appid:140,
		category:"game",
		
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/towers-n-trolls_cgkvd.html",
		playStore:"market://details?id=project.android.ftdjni",
		starRating:"4.7",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"Towers N' Trolls",
		img:"http://tiptoptale.com/data/140-towers-n-trolls/main.jpg",
		desc:"Towers & Trolls is indeed a very good defense game, suited for every defense gamer or even for those who want to get themselves into the genre.",
		video:"https://www.youtube.com/embed/w2ksr043BGQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:139,
		category:"app",
		
		developer:"http://www.theverge.com/contact-the-verge",
		reviews:"http://www.androidtapp.com/the-verge/",
		playStore:"market://details?id=com.verge.android",
		starRating:"4.3",
		licence:"Free",
		downloads:"100,000+",
		version:"2.2",
		
		name:"The Verge",
		img:"http://tiptoptale.com/data/139-the-verge/main.jpg",
		desc:"The Verge is a comprehensive and frequently updated source for tech news. Launched only at the end of 2011, it covers everything from the latest Android headlines, Microsoft, Apple, through to photography, gadgets, gaming, culture and the Internet.",
	},
	
	{
		appid:138,
		category:"app",
		
		top:"developer",
		developer:"http://www.skype.com/en/",
		reviews:"http://www.engadget.com/2013/10/09/skype-android-video-quality-tablet-ui-update/",
		playStore:"market://details?id=com.skype.raider",
		starRating:"4.0",
		licence:"Free",
		downloads:"100,000,000+",
		
		name:"Skype - free IM & video calls",
		img:"http://tiptoptale.com/data/138-skype/main.jpg",
		desc:"The latest version of Skype for Android has scored two major updates: better video quality and a new tablet interface. The VoIP provider claims you can now enjoy video chats with up to four times the resolution of older Skype versions, whether you're using a phone or a slate.",
		video:"https://www.youtube.com/embed/QHIzzQB4Lew?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:137,
		category:"game",
		
		developer:"http://www.defiantdev.com/ski-safari.php",
		reviews:"http://www.pocket-lint.com/news/117494-ski-safari-android-app-review",
		playStore:"market://details?id=com.DefiantDev.SkiSafari",
		starRating:"4.8",
		licence:"Free",
		downloads:"100,000+",
		
		name:"Ski Safari",
		img:"http://tiptoptale.com/data/137-ski-safari/main.jpg",
		desc:"Ski Safari sees your character, Sven, trying to escape from an avalanche sweeping through the mountains. It's a platform game that sees this skiing action running across the screen constantly. The aim is to stay ahead of the avalanche, which ends the game, to accrue distance and complete various objectives along the way.",
		video:"https://www.youtube.com/embed/XHY3fS05kiE?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:136,
		category:"app",
		
		developer:"http://support.ted.com/",
		reviews:"http://www.appszoom.com/android_applications/education/ted_cbsrx.html",
		playStore:"market://details?id=com.ted.android",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"TED",
		img:"http://tiptoptale.com/data/136-ted/main.jpg",
		desc:"TED is the official Android app of the well-known global set of conferences. You can find more than 1200 TEDTalk videos and audios and more are added every week. You can share it, add to watch it later or even download the talk to your SDcard.",
		video:"https://www.youtube.com/embed/1nJutfZEcYw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:135,
		category:"app",
		
		playStore:"market://details?id=com.levelokment.storageanalyser",
		starRating:"4.7",
		licence:"Free",
		downloads:"100,000+",
		version:"2.1",
		
		name:"Storage Analyser",
		img:"http://tiptoptale.com/data/135-storage-analyser/main.jpg",
		desc:"Analyses Sdcard, external Sdcards, USB storage devices, system partitions (disabled by default)... See the space used by applications using App2SD. Filter out the content you are aware of to make the rest more noticeable.",
	},
	
	{
		appid:134,
		category:"app",
		
		developer:"http://www.speedtest.net/mobile/",
		reviews:"http://www.androidtapp.com/speedtest-net-speed-test/",
		playStore:"market://details?id=org.zwanoo.android.speedtest",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Speedtest",
		img:"http://tiptoptale.com/data/134-speedtest/main.jpg",
		desc:"Speedtest.net is now available as a free Android application with no advertising! Use it to measure the network speed of your Android device. With over a million tests performed every day, Speedtest.net is the ultimate resource for bandwidth testing and related information.",
	},
	
	{
		appid:133,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"https://firemonkeys.zendesk.com/home",
		reviews:"http://real-racing-3.appszoom.com/android",
		playStore:"market://details?id=com.ea.games.r3_row",
		starRating:"4.0",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.3",
		
		name:"Real Racing 3",
		img:"http://tiptoptale.com/data/133-real-racing3/main.jpg",
		desc:"Real Racing 3 is one of the most anticipated car racing simulator for Android. It features well-crafted and realistic environment that perfectly recreates official tracks: Indianapolis, Laguna Seca, Silverstone and many many more. Obviously, graphics can always be enhanced but that's by far the strongest point of the game.",
		video:"https://www.youtube.com/embed/kUDkR3ubuqk?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:132,
		category:"app",
		
		developer:"http://developers.aviary.com/docs/android",
		reviews:"http://www.pocket-lint.com/news/121122-app-of-the-day-photo-editor-by-aviary-review-android",
		playStore:"market://details?id=com.aviary.android.feather",
		starRating:"4.7",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.3",
		
		name:"Photo Editor by Aviary",
		img:"http://tiptoptale.com/data/132-photo-editor-by-aviary/main.jpg",
		desc:"Aviary is a powerful photo editor which they created because they wanted a quick and easy way to edit our photos on the go with no fuss. They've included all the tools you need, in a super intuitive interface so you can get right to editing.",
	},
	
	{
		appid:131,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://about.king.com/games/pet-rescue-saga/faq",
		reviews:"http://www.pocket-lint.com/news/121856-app-of-the-day-pet-rescue-saga-review-iphone",
		playStore:"market://details?id=com.king.petrescuesaga",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Pet Rescue Saga",
		img:"http://tiptoptale.com/data/131-pet-rescue-saga/main.jpg",
		desc:"Pet Rescue Saga, the latest tile from King - makers of app heavyweight Candy Crush Saga - has landed to eat into your every minute. It's a good-looking and well-made puzzle game that's taxing and rewarding in equal measure.",
	},
	
	{
		appid:130,
		category:"game",
		
		top:"developer",
		developer:"http://www.game-lion.com/",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/monster-shooter-2_hewhk.html",
		playStore:"market://details?id=com.gamelion.ms2",
		starRating:"4.5",
		licence:"Free",
		downloads:"500,000+",
		version:"2.3",
		
		name:"Monster Shooter 2",
		img:"http://tiptoptale.com/data/130-monster-shooter2/main.jpg",
		desc:"Of course, there's something better than shooting monsters down, and that's being a monster yourself and armed with all kinds of monster weapons. As an upgrade of its predecessor, Monster Shooter 2 is a one versus the world shoot them up whose most significant feature is the vast amount of enemies appearing at the same time on screen.",
		video:"https://www.youtube.com/embed/sOkyc9POSUY?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:129,
		category:"app",
		
		top:"developer",
		developer:"http://www.linkedin.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2417904,00.asp",
		playStore:"market://details?id=com.linkedin.android",
		starRating:"4.1",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"LinkedIn",
		img:"http://tiptoptale.com/data/129-linkedin/main.jpg",
		desc:"LinkedIn remains one of the most important online networks for professionals to join, and the supremely thorough Android app helps you stay on top of business relationships like none other.",
	},
	
	{
		appid:128,
		category:"app",
		
		developer:"http://line.naver.jp/en/",
		reviews:"http://line.appszoom.com/android",
		playStore:"market://details?id=jp.naver.line.android",
		starRating:"4.2",
		licence:"Free",
		downloads:"100,000,000+",
		version:"2.1",
		
		name:"LINE: Free Calls & Messages",
		img:"http://tiptoptale.com/data/128-line/main.jpg",
		desc:"LINE is one of the few or possibly the only app that has been able to compete with the most popular communication tool for mobile phones; it already has 75 million users and everybody seems to be talking about it. What's new? Well, besides the icons and stickers, which are cute but not really essential, the possibility of calling for free is one good reason to try it out.",
		video:"https://www.youtube.com/embed/gyAJLQ6En7A?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:127,
		category:"game",
		
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/tiny-robots-beta_bhusv.html",
		playStore:"market://details?id=com.RunnerGames.game.TinyRobots_New",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"1.6",
		
		name:"Tiny Robots",
		img:"http://tiptoptale.com/data/127-tiny-robots/main.jpg",
		desc:"Tilt your phone and control the tank so it kills the aliens robots. This addictive game includes 30 levels that will increase in difficulty and challenge. ",
	},
	
	{
		appid:126,
		live:"wallpaper",
		
		top:"developer",
		playStore:"market://details?id=fishnoodle.storm_free",
		starRating:"4.2",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Thunderstorm Free Wallpaper",
		img:"http://tiptoptale.com/data/126-thunderstorm-lpw/main.jpg",
		desc:"A spectacular backdrop of storm clouds, lightning, and rain! Not a movie, with full support for landscape mode and home-screen switching! Works on both phones and tablets!",
		video:"https://www.youtube.com/embed/1G-CC2GatKg?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:125,
		category:"game",
		
		top:"developer",
		developer:"http://global.com2us.com/",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/tower-defense_ceets.html",
		playStore:"market://details?id=com.com2us.towerdefense.normal.freefull.google.global.android.common",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Tower Defense&reg;",
		img:"http://tiptoptale.com/data/125-tower-defense/main.jpg",
		desc:"High-end tech weapons are at your disposal. Place them strategically to avoid any creature reaching your headquarters or they'll get the control of the whole field. Use your resources to buy new units and upgrade them, otherwise you won't be able to stop the increasing hordes of aliens.",
		video:"https://www.youtube.com/embed/5tMyp1ZQOuM?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:124,
		live:"wallpaper",
		
		top:"developer",
		developer:"http://www.cellfishmedia.com/",
		playStore:"market://details?id=com.cellfish.livewallpaper.marvel_avengers",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"The Avengers Live Wallpaper",
		img:"http://tiptoptale.com/data/124-the-avengers-lpw/main.jpg",
		desc:"Download the free Avengers Live Wallpaper featuring the superhero team's logo hovering over a destroyed New York City street with Hulk and Thor in the foreground!",
		video:"https://www.youtube.com/embed/yMjlk-kqpL0?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:123,
		category:"game",
		newApp:"new",
		
		developer:"http://super-cricket.com/",
		reviews:"http://www.appszoom.com/android_games/sports_games/stick-cricket_bsgan.html",
		playStore:"market://details?id=com.nklsrh.supercricketfree",
		starRating:"4.3",
		licence:"Free",
		downloads:"50,000+",
		version:"2.2",
		
		name:"Super Cricket",
		img:"http://tiptoptale.com/data/123-super-cricket/main.jpg",
		desc:"Play the famous sport and beat players around the world. Stick Cricket is a new game for Android that will surely make cricket fans happy. Don't worry, even if you don't like the sport, you can still enjoy it. If you like sport apps, this is a good one.",
		video:"https://www.youtube.com/embed/1LnqQC3jW7Y?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:122,
		category:"app",
		
		top:"developer",
		developer:"http://www.stumbleupon.com/",
		reviews:"http://www.androidtapp.com/stumbleupon/",
		playStore:"market://details?id=com.stumbleupon.android.app",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"StumbleUpon",
		img:"http://tiptoptale.com/data/122-stumble-upon/main.jpg",
		desc:"StumbleUpon is a popular and fun Internet exploration website that presents random websites based upon your own interests. Simply create an account, list your interests and 'Stumble' upon interesting content. The website then sends you to various websites that are relevant to you. The StumbleUpon app now lets you do this from your Android device.",
	},
	
	{
		appid:121,
		category:"game",
		
		developer:"http://www.auxbrain.com/",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/zombie-highway_dbhtq.html",
		playStore:"market://details?id=com.auxbrain.zombie_highway",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.3",
		
		name:"Zombie Highway",
		img:"http://tiptoptale.com/data/121-zombie-highway/main.jpg",
		desc:"Head down an infinite desert highway swarmed with zombies! Navigate obstacles while scraping and shooting off the undead clinging to your car, in one of the most addictive zombie survival games!",
	},
	
	{
		appid:120,
		category:"game",
		
		reviews:"http://zombie-smasher.en.softonic.com/android",
		playStore:"market://details?id=com.moistrue.zombiesmasher",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Zombie Smasher",
		img:"http://tiptoptale.com/data/120-zombie-smasher/main.jpg",
		desc:"Zombie Smasher is a hilarious and fun game in which you need to kill zombies by squishing them with your fingers.",
	},
	
	{
		appid:119,
		category:"game",
		
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/zombie-evil_dcjjn.html",
		playStore:"market://details?id=com.feelingtouch.gnz",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Zombie Evil",
		img:"http://tiptoptale.com/data/119-zombie-evil/main.jpg",
		desc:"Zombies, yes, zombies again. You all like them, don't you? If you don't, no worries, Zombie Evil isn't about showing any love for them, but pumping them full of lead. Zombie Evil is set in cartoony graphics that makes it more attractive for a wider audience.",
	},
	
	{
		appid:118,
		category:"app",
		
		developer:"http://www.zausan.com/index.php?lang=en",
		playStore:"market://details?id=zausan.zdevicetest",
		starRating:"4.3",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.0",
		
		name:"Z - Device Test",
		img:"http://tiptoptale.com/data/118-z-device-test/main.jpg",
		desc:"Z-DeviceTest lets you check your Android device sensors 'health' in an intuitive and comprehensive way offering in-depth analysis all the characteristics of your Smartphone.",
		video:"https://www.youtube.com/embed/OJ1z6KF4dUo?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:117,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://global.com2us.com/",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/swing-shot-hd_clyvj.html",
		playStore:"market://details?id=com.com2us.monkeybattle.normal.freefull.google.global.android.common",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Swing Shot HD",
		img:"http://tiptoptale.com/data/117-swing-shot/main.jpg",
		desc:"One of the most enjoyable games we have played, perfect graphics and addictive gameplay. There are four worlds and 48 levels in total, we hope they keep increasing that number, although you can always try to improve your score in every one of them.",
		video:"https://www.youtube.com/embed/Xe56nVtHcYA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:116,
		live:"wallpaper",
		
		playStore:"market://details?id=com.teragon.skyatdawnlw.lite",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Sun Rise Free Live Wallpaper",
		img:"http://tiptoptale.com/data/116-sun-rise-LPW/main.jpg",
		desc:"This live wallpaper features tree silhouette, on top of a morning sky background with soft cloud moving, birds flying, and rainbow appearing occasionally.",
	},
	
	{
		appid:115,
		category:"game",
		
		top:"developer",
		developer:"http://www.sticksports.com/",
		reviews:"http://www.appszoom.com/android_games/sports_games/stick-cricket_bsgan.html",
		playStore:"market://details?id=com.sticksports.stickcricket",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		
		name:"Stick Cricket",
		img:"http://tiptoptale.com/data/115-stick-cricket/main.jpg",
		desc:"Stick Cricket is a new game for Android that will surely make cricket fans happy. Don't worry, even if you don't like the sport, you can still enjoy it. If you like sport apps, this is a good one.",
		video:"https://www.youtube.com/embed/dzqT7RsoLIw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:114,
		category:"app",
		
		top:"developer",
		developer:"http://www.splashtop.com/",
		reviews:"http://www.appszoom.com/android_applications/business/splashtop-2-remote-desktop_cqslx.html",
		playStore:"market://details?id=com.splashtop.remote.pad.v2",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Splashtop 2 Remote Desktop",
		img:"http://tiptoptale.com/data/114-splashtop2-remote-desktop/main.jpg",
		desc:"Splashtop 2 HD is a newer version of Splashtop Remote Desktop. It comes with new features like: full access and compatibility with all your programs (PWP, Outlook, Safari, Firefox, and other PC/Mac apps); more music and video formats supported including flash. Play both 3D games and flash games.",
		video:"https://www.youtube.com/embed/Z82ezbykOn4?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:113,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://www.game-lion.com/",
		reviews:"http://www.androidtapp.com/speedx-3d/",
		playStore:"market://details?id=com.gamelion.speedx3d",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"SpeedX 3D",
		img:"http://tiptoptale.com/data/113-speedx-3d/main.jpg",
		desc:"Speedx 3D delivers stunning 3D accelerometer controlled tunnel experience. Test your reflex in ultimate speed challenge with smooth 3D graphics. Compete with other players through global online high score board.",
	},
	
	{
		appid:112,
		category:"app",
		
		top:"developer",
		developer:"http://www.smule.com/",
		reviews:"http://www.androidtapp.com/songify/",
		playStore:"market://details?id=com.smule.songify",
		starRating:"4.0",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Songify",
		img:"http://tiptoptale.com/data/112-songify/main.jpg",
		desc:"Songify, the hugely successful music app that turns your speech into singing, is now available on Android. Whether you like the sound of your own voice, or are generally tone-deaf, Songify is loads of fun and easy to use.",
		video:"https://www.youtube.com/embed/aVmirA_JxGY?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:111,
		category:"app",
		
		top:"developer",
		developer:"http://www.soundhound.com/",
		reviews:"http://www.androidtapp.com/soundhound/",
		playStore:"market://details?id=com.melodis.midomiMusicIdentifier.freemium",
		starRating:"4.4",
		licence:"Free",
		downloads:"50,000,000+",
		
		name:"SoundHound",
		img:"http://tiptoptale.com/data/111-sound-hound/main.jpg",
		desc:"SoundHound. World's fastest music recognition: name a song playing from a speaker in just 4 seconds - works even if you sing or hum.",
		video:"https://www.youtube.com/embed/A6sk8T-_jmQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:110,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://www.glu.com/",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/samurai-vs-zombies-defense_ccgot.html",
		playStore:"market://details?id=com.glu.samuzombie2",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Samurai vs Zombies Defense 2",
		img:"http://tiptoptale.com/data/110-samurai-vs-zombies-defense2/main.jpg",
		desc:"It's a new concept of tower-defense game. The main difference is that instead of placing different units through the field, you manage only one Samurai that eventually will be helped by some allies (as special attack). Likewise, he will use power-ups, special attacks and weapons that can be upgraded after each level.",
		video:"https://www.youtube.com/embed/A6sk8T-_jmQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:109,
		category:"game",
		
		developer:"http://www.animoca.com/en/",
		playStore:"market://details?id=com.animoca.google.robo5",
		starRating:"4.3",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.0",
		
		name:"Robo5",
		img:"http://tiptoptale.com/data/109-robo5/main.jpg",
		desc:"Enjoy hours of mind-bending fun as you help Robo #5 escape from the freezing cold lab where our mechanical hero begins his adventure. Marvel at the steampunk charm as you guide Robo on a journey of self discovery and puzzle-solving!",
		video:"https://www.youtube.com/embed/36MQZHkEm-k?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:108,
		category:"game",
		
		top:"developer",
		developer:"http://www.gameloft.com/?sk",
		reviews:"http://www.appszoom.com/android_games/sports_games/real-football-2012_bzkkj.html",
		playStore:"market://details?id=com.gameloft.android.ANMP.GloftR2HM",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Real Football 2012",
		img:"http://tiptoptale.com/data/108-real-football-2012/main.jpg",
		desc:"Real Football 2012 is full of little details that soccer fans will enjoy much, as 'did you know...?' sentences and quotes at loadings, five difficulty settings from junior to legend, intro movies and trivia questions to earn more coins exchangeable for fields, leagues and improvements for players' prowess.",
		video:"https://www.youtube.com/embed/VMfLXTi8Pls?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:107,
		category:"game",
		
		top:"developer",
		developer:"http://www.gamevil.com/",
		reviews:"http://www.appszoom.com/android_games/sports_games/punch-hero_cqncu.html",
		playStore:"market://details?id=com.gamevil.punchhero.glo",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.3",
		
		name:"Punch Hero",
		img:"http://tiptoptale.com/data/107-punch-hero/main.jpg",
		desc:"Presented in hilarious 3D, Punch Hero is addictive as it is challenging. With multiple modes of gameplay and customizable options, Punch Hero offers hours of endless boxing action!",
		video:"https://www.youtube.com/embed/-bEwfnQFJJg?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:106,
		category:"app",
		
		developer:"http://pose.com/",
		reviews:"http://www.appszoom.com/android_applications/social/pose_bklhf.html",
		playStore:"market://details?id=com.pose",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Pose",
		img:"http://tiptoptale.com/data/106-pose/main.jpg",
		desc:"Pose: Share & Discover Style is a social network that revolves about one popular topic: Fashion. Discover new styles and let them others discover yours by posting pictures of your outfits.",
		video:"https://www.youtube.com/embed/qdIcz07MpO8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:105,
		live:"wallpaper",
		
		playStore:"market://details?id=com.androidwasabi.livewallpaper.s4leaf",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Galaxy S4 Leaf Live Wallpaper",
		img:"http://tiptoptale.com/data/105-galaxy-s4-leaf-lpw/main.jpg",
		desc:"Download free Galaxy S4 Leaf live wallpaper with floating leaves. Inspired by original Samsung Galaxy S4 leaf background. Now with 'Water Droplet' effect!",
		video:"https://www.youtube.com/embed/cAHScrwiiTc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:104,
		category:"app",
		
		top:"developer",
		developer:"http://help.getpocket.com/",
		reviews:"http://www.androidtapp.com/read-it-later-pro/",
		playStore:"market://details?id=com.ideashower.readitlater.pro",
		starRating:"4.7",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Pocket",
		img:"http://tiptoptale.com/data/104-pocket/main.jpg",
		desc:"Pocket lets you save webpages to read later, even without an Internet connection. If you like to keep up to date with the latest news: whether it be Android news, politics, sports, entertainment or perhaps the latest blog by that guy that does that thing... or web page, then Pocket might just be your thing.",
		video:"https://www.youtube.com/embed/bfob4cBfLOA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:103,
		category:"app",
		
		developer:"http://picsart.com",
		reviews:"http://www.appszoom.com/android_applications/photography/picsart-photo-studio_bowom.html",
		playStore:"market://details?id=com.picsart.studio",
		starRating:"4.7",
		licence:"Free",
		downloads:"50,000,000+",
		
		name:"PicsArt - Photo Studio",
		img:"http://tiptoptale.com/data/103-pics-art/main.jpg",
		desc:"This is one app that has been developed to edit the photo quickly and share it on the social networks. The amount of social networks integrated is impressive: Facebook, Flickr, Picasa, Twitter, Fousquare, Tumbrl, Blogger, Wordpress... among others.",
		video:"https://www.youtube.com/embed/WYbFtMwzNeY?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:102,
		category:"game",
		multi:"multi",
		
		developer:"http://www.gamegou.com/",
		reviews:"http://www.androidtapp.com/perfect-kick/",
		playStore:"market://details?id=com.gamegou.PerfectKick.google",
		starRating:"4.7",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Perfect Kick",
		img:"http://tiptoptale.com/data/102-perfect-kick/main.jpg",
		desc:"Perfect Kick is a fun, addictive and social football (soccer) game. Gameplay consists of taking and defending penalty kicks against AI and/or online players in a shootout. Perfect Kick is simple to play and ideal if you like football and competitive gameplay.",
		video:"https://www.youtube.com/embed/s65cYa2U-UA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1&authuser=0",
	},
	
	{
		appid:101,
		category:"app",
		
		developer:"https://ustream.zendesk.com/home",
		reviews:"http://www.appszoom.com/android_applications/multimedia/ustream_pceh.html",
		playStore:"market://details?id=tv.ustream.ustream",
		starRating:"3.9",
		licence:"Free",
		downloads:"5,000,000+",
		
		name:"Ustream",
		img:"http://tiptoptale.com/data/101-ustream/main.jpg",
		desc:"Ustream is a web service that allows you to watch and create channels to broadcast video content. Since the spreading of smartphones all around the World, it has taken a new direction: its Android app allows you to watch and broadcast live videos from your smartphone over 3G/4G and WiFi.",
	},
	
	{
		appid:100,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://www.viber.com/",
		reviews:"http://www.androidtapp.com/viber-free-calls-messages/",
		playStore:"market://details?id=com.viber.voip",
		starRating:"4.4",
		licence:"Free",
		downloads:"100,000,000+",
		version:"2.2",
		
		name:"Viber",
		img:"http://tiptoptale.com/data/100-viber/main.jpg",
		desc:"Viber: Free Calls & Messages is an Android app that lets you make free phone calls and send free text messages to other users that have Viber installed. When you use Viber, your phone calls to any other Viber user are free, and the sound quality is much better than a regular call. You can call any Viber user, anywhere in the world, for free.",
	},
	
	{
		appid:99,
		category:"game",
		
		top:"developer",
		developer:"http://nekki.com",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/vector_fjzgf.html",
		playStore:"market://details?id=com.nekki.vector",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Vector",
		img:"http://tiptoptale.com/data/99-vector/main.jpg",
		desc:"Vector by Nekki is one of those games you have to had installed on your Android, because it's better than a time waster, it's a life saver. It's one of those games you always appreciate to play when there isn't anything else to do.",
		video:"https://www.youtube.com/embed/vAbN2dIdOvE?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:98,
		category:"app",
		
		developer:"http://www.ucweb.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2400552,00.asp",
		playStore:"market://details?id=com.UCMobile.intl",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"UC Browser for Android",
		img:"http://tiptoptale.com/data/98-uc-browser/main.jpg",
		desc:"Best Browser for Downloads! Unlike Dolphin and Opera, UC Browser supports simultaneous downloads and has a clean, well-organized download manager for all your files, including pages for offline reading.",
		video:"https://www.youtube.com/embed/UAHdTIZbyrA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:97,
		live:"wallpaper",
		
		developer:"http://www.kivano.net/",
		playStore:"market://details?id=net.kivano.ubuntulwp",
		starRating:"4.4",
		licence:"Free",
		downloads:"100,000+",
		version:"2.2",
		
		name:"Ubuntu Live Wallpaper",
		img:"http://tiptoptale.com/data/97-ubuntu-lwp/main.jpg",
		desc:"Are you a fan of Ubuntu? Waiting for Ubuntu Phone? So do we! :) If you can wait no more, we have small substitute for your android phone! You can enjoy a live wallpaper that is similar to Ubuntu Phone 'Welcome Screen' from Canonical.",
	},
	
	{
		appid:96,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://www.truecaller.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2426585,00.asp",
		playStore:"market://details?id=com.truecaller",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Truecaller - Caller ID & Block",
		img:"http://tiptoptale.com/data/96-truecaller/main.jpg",
		desc:"Truecaller sells itself as a social, crowd-sourced call and SMS blocking app, boasting over 25 million users. The free version of the app also has a host of advanced features.",
	},
	
	{
		appid:95,
		category:"app",
		
		top:"developer",
		developer:"http://tumblr.com",
		reviews:"http://reviews.cnet.com/software/tumblr-android/4505-3513_7-35660278.html",
		playStore:"market://details?id=com.tumblr",
		starRating:"4.3",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Tumblr",
		img:"http://tiptoptale.com/data/95-tumblr/main.jpg",
		desc:"With the newly updated Tumblr for Android, using the megapopular blogging platform cum social network while on the go is a snap. The app lets you create and publish posts, read through posts by bloggers you follow, and manage your account settings, all through a beautiful interface with crisp images and dead simple navigation.",
	},
	
	{
		appid:94,
		category:"game",
		
		top:"developer",
		developer:"http://www.deemedya.com/",
		reviews:"http://www.appszoom.com/android_games/racing/trial-xtreme-2-free_bnaef.html",
		playStore:"market://details?id=com.galapagossoft.trialx2_gl2_demo",
		starRating:"4.3",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.1",
		
		name:"Trial Xtreme 2 Free",
		img:"http://tiptoptale.com/data/94-trail-xtreme2/main.jpg",
		desc:"Ride your bike keeping your balance to surpass obstacles. Trial games have always been very popular among gamers. Their mix of concentration, essay and error and a little bit of luck make these games very addictive to certain kind of players.",
		video:"https://www.youtube.com/embed/nMNMDzx7R2g?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:93,
		category:"app",
		widget:"widget",
		
		reviews:"http://www.appszoom.com/android_applications/tools/tiny-flashlight-led_imsk.html",
		playStore:"market://details?id=com.devuni.flashlight",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000,000+",
		
		name:"Tiny Flashlight + LED",
		img:"http://tiptoptale.com/data/93-tiny-flashlight/main.jpg",
		desc:"Tiny Flashlight is an app that adds a new functionality to our Android: it turns it into a lamp. It uses the LED camera to do it. So, it's useless for those devices with no camera flash (Samsung Galaxy S among others).",
		video:"https://www.youtube.com/embed/Tc7wILRPcOk?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:92,
		live:"wallpaper",
		
		playStore:"market://details?id=com.chickenbellyfinn.trianglatorlive",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000+",
		version:"2.2",
		
		name:"Nexus Triangles LWP Free",
		img:"http://tiptoptale.com/data/92-nexus-triangles-lwp/main.jpg",
		desc:"A live wallpaper that aims to resemble one of the default Android 4.2 Jelly Bean default wallpapers. It is highly configurable with custom color themes, sizes, and other settings.",
		video:"https://www.youtube.com/embed/0IgBxLP1T7g?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:91,
		category:"game",
		
		developer:"http://www.wavecade.com/",
		reviews:"http://www.appszoom.com/android_games/casual/my-paper-plane-2-3d_irkl.html",
		playStore:"market://details?id=com.wavecade.mypaperplaneplanetlite",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000+",
		version:"2.2",
		
		name:"My Paper Plane 3 (3D) Lite",
		img:"http://tiptoptale.com/data/91-my-paper-plane3/main.jpg",
		desc:"My Paper Plane 2 is one of those simple games that, when you least expect it, gets you hooked for a long while. Graphics are right and proper, sound is right and proper and, in fact, everything's right and proper.",
		video:"https://www.youtube.com/embed/F69-AF6gFPk?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:90,
		category:"app",
		
		developer:"http://www.moshcam.com/",
		reviews:"http://mashable.com/2012/03/22/moshcam-concerts-app/",
		playStore:"market://details?id=com.moshcam",
		starRating:"4.7",
		licence:"Free",
		downloads:"5,000+",
		version:"2.2",
		
		name:"Moshcam",
		img:"http://tiptoptale.com/data/90-moshcam/main.jpg",
		desc:"Moshcam gives you instant access to more than 1,200 hours of live music from a roster of bands including The Decemberists, Jane's Addiction, Blondie, Public Enemy, and Flogging Molly. The shows can be watched on-demand on your Android, iPad or iPod touch as well as played on your television through Apple TV.",
	},
	
	{
		appid:89,
		category:"app",
		
		developer:"http://mrnumber.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2426541,00.asp",
		playStore:"market://details?id=com.mrnumber.blocker",
		starRating:"4.3",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.1",
		
		name:"Mr. Number-Block calls, texts",
		img:"http://tiptoptale.com/data/89-mr-number-block-calls/main.jpg",
		desc:"Just because you have a smartphone doesn't mean you want people calling you all the time. Mr. Number! A handy little app that makes it easy to block calls and text messages from specific numbers, area codes, code prefixes, or just everyone.",
	},
	
	{
		appid:88,
		category:"game",
		
		playStore:"market://details?id=com.Wuzla.game.Block_AD",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Mr. Block Free",
		img:"http://tiptoptale.com/data/88-mr-block-free/main.jpg",
		desc:"Mr's Block is a challenging game. You need to color the same color of the Block on the box, it looks easy, is not that simple.",
	},
	
	{
		appid:87,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://withbuddies.com/",
		reviews:"http://www.gamezebo.com/games/mini-golf-matchup/review",
		playStore:"market://details?id=com.scopely.minigolf.free",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Mini Golf MatchUp&trade;",
		img:"http://tiptoptale.com/data/87-mini-golf-machup/main.jpg",
		desc:"Mini Golf MatchUp brings the competitive nature of putt-putt to your touch screen devices, embracing a style of turn-based multiplayer that mobile gamers have come to know and love in recent years.",
		video:"https://www.youtube.com/embed/RQdaZvDCxvI?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:86,
		category:"game",
		
		developer:"http://game.alpersarikaya.com/",
		reviews:"http://www.androidtapp.com/manuganu/",
		playStore:"market://details?id=com.Alper.Manuganu",
		starRating:"4.7",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"ManuGanu",
		img:"http://tiptoptale.com/data/86-manuganu/main.jpg",
		desc:"MANUGANU is a platform running game where you control a little adventurous boy to dodge all sorts of dangerous obstacles by jumping, flipping, swinging, sliding, gliding and bashing your way to the finish line unscathed and in record times while collecting blue coins and medallions.",
		video:"https://www.youtube.com/embed/BLU_80IQtTM?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:85,
		category:"game",
		newApp:"new",
		
		developer:"http://www.kiterungame.com/",
		playStore:"market://details?id=com.game.kiterun",
		starRating:"4.4",
		licence:"Free",
		downloads:"5,000+",
		version:"2.2",
		
		name:"Kite Run",
		img:"http://tiptoptale.com/data/85-kite-run/main.jpg",
		desc:"This game is based on a real life wartime challenge on how far kites can keep flying, with the right skill and enough nerve. Now, it's your turn. See how far your kite can go.",
	},
	
	{
		appid:84,
		category:"app",
		
		developer:"http://www.interactiveuniverse.net/",
		reviews:"http://www.techhive.com/article/250433/lapse_it_pro_review_one_of_the_best_time_lapse_photography_apps_available_for_android.html",
		playStore:"market://details?id=com.ui.LapseIt",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"1.6",
		
		name:"Lapse It &bull; Time Lapse &bull; Lite",
		img:"http://tiptoptale.com/data/84-lapse-it/main.jpg",
		desc:"Time-lapse photography allows you to watch objects and scenes change over time. Lapse It Pro for Android lets you use your phone's built-in camera to create your very own time-lapsed scenes.",
		video:"https://www.youtube.com/embed/oTwbP1vNvqk?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:83,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://pikpok.com/",
		reviews:"http://getandroidstuff.com/into-the-dead-android-game-download-run-away-form-zombies/",
		playStore:"market://details?id=com.sidheinteractive.sif.DR",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.3",
		
		name:"Into the Dead",
		img:"http://tiptoptale.com/data/83-into-the-dead/main.jpg",
		desc:"Into the Dead Android game comes with a different gameplay which will not bore you quickly like other zombie game available for Android. Into the Dead is an awesome Temple Run style game where you will face zombies while running for your life.",
		video:"https://www.youtube.com/embed/fhx0MeC13ws?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:82,
		category:"app",
		newApp:"new",
		
		developer:"http://www.healthkartplus.com/",
		reviews:"http://playboard.me/android/apps/com.aranoah.healthkart.plus",
		playStore:"market://details?id=com.aranoah.healthkart.plus",
		starRating:"4.5",
		licence:"Free",
		downloads:"50,000+",
		version:"2.2",
		
		name:"Cheaper Medicine - HealthKart+",
		img:"http://tiptoptale.com/data/82-healthkartplus-search-medicine/main.jpg",
		desc:"HealthKartPlus is India's First & Only Comprehensive Generic Drug Search Engine. *Now Order Medicines Online from the App (in India only).",
		video:"https://www.youtube.com/embed/0evwG5lo9Ok?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:81,
		category:"app",
		newApp:"new",
		
		reviews:"http://www.appbrain.com/app/indian-caller-info/ardent.androidapps.callerinfo.views",
		playStore:"market://details?id=ardent.androidapps.callerinfo.views",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		
		name:"Indian Caller Info",
		img:"http://tiptoptale.com/data/81-indian-caller-info/main.jpg",
		desc:"Indian Caller info application will give you the flexibility to know from which place the call is coming at the time when cell is ringing.",
	},
	
	{
		appid:80,
		live:"wallpaper",
		
		developer:"http://www.tribok.com/",
		playStore:"market://details?id=com.tribok.android.livewallpaper.icsclassic.lite",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000+",
		version:"2.1",
		
		name:"ICS Phase Beam Live Wallpaper",
		img:"http://tiptoptale.com/data/80-ics-phase-beam-live-wallpaper/main.jpg",
		desc:"This is a clone of the new Phase Beam Live Wallpaper from Android 4.0 Ice Cream Sandwich. In comparison with the original Wallpaper, this one has additionally a cool 3D scroll effect.",
		video:"https://www.youtube.com/embed/NhCXnWeXDT0?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:79,
		category:"game",
		
		developer:"http://www.fingersoft.net/",
		reviews:"http://www.androidtapp.com/hill-climb-racing/",
		playStore:"market://details?id=com.fingersoft.hillclimb",
		starRating:"4.7",
		licence:"Free",
		downloads:"50,000,000+",
		version:"2.2",
		
		name:"Hill Climb Racing",
		img:"http://tiptoptale.com/data/79-hill-climb-racing/main.jpg",
		desc:"Hill Climb Racing is a simple yet addictive racing game on Android. The game is a 2D physics-based racer where you have to traverse a number of hills and bumps for as long as possible.",
		video:"https://www.youtube.com/embed/mh3y5qH1Mz8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:78,
		category:"game",
		multi:"multi",
		
		developer:"http://www.angrymobgames.com/",
		reviews:"http://www.androidguys.com/2011/04/28/app-review-guerrilla-bob-android/",
		playStore:"market://details?id=com.angrymobgames.guerrillaboblite",
		starRating:"4.2",
		licence:"Free",
		downloads:"500,000+",
		version:"2.0",
		
		name:"Guerrilla Bob LITE",
		img:"http://tiptoptale.com/data/78-guerrilla-bob-lite/main.jpg",
		desc:"Guerrilla Bob by Angry Mob Games is the ultimate 3rd person shooter game for Android. Not only does this game become very addicting, the graphics are unbelievable! You can play multiplayer which allows cross-platform gaming! This means you can challenge Mac users, Windows users, and even iOs users.",
		video:"https://www.youtube.com/embed/TntE55pN_Qc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:77,
		category:"game",
		multi:"multi",
		
		playStore:"market://details?id=com.feelingtouch.strikeforce",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Gun & Blood",
		img:"http://tiptoptale.com/data/77-gun-and-blood/main.jpg",
		desc:"Listen up man; it is time to kill all terrorists. You are one of the strike forces to destroy anything blocked on your way.",
	},
	
	{
		appid:76,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://www.gameloft.com",
		reviews:"http://www.androidpolice.com/2011/07/29/review-gt-racing-motor-academy-free-makes-me-wonder-if-gameloft-is-actually-trying-anymore/",
		playStore:"market://details?id=com.gameloft.android.ANMP.GloftGTFM",
		starRating:"4.7",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"GT Racing: Motor Academy Free+",
		img:"http://tiptoptale.com/data/76-gt-racing-motor-academy/main.jpg",
		desc:"At its core, it is a bastardized version of Gran Turismo that has been stuffed with micropayments to specifically eliminate the charm of Gran Turismo.",
		video:"https://www.youtube.com/embed/2Z9OPICdgoA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:75,
		category:"game",
		multi:"multi",
		
		playStore:"market://details?id=com.natenai.glowhockey",
		starRating:"4.4",
		licence:"Free",
		downloads:"50,000,000+",
		version:"1.6",
		
		name:"Glow Hockey",
		img:"http://tiptoptale.com/data/75-glow-hockey/main.jpg",
		desc:"Glow Hockey delivers a new style of hockey game. Easy to play, hard to master. Challenge yourself with the computer opponents!",
		video:"https://www.youtube.com/embed/IIFG5HCyL4Q?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:74,
		category:"game",
		
		top:"developer",
		developer:"http://www.backflipstudios.com/",
		reviews:"http://paper-toss-20.en.softonic.com/android",
		playStore:"market://details?id=com.backflipstudios.android.papertoss2",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.1",
		
		name:"Paper Toss 2.0",
		img:"http://tiptoptale.com/data/74-paper-toss2/main.jpg",
		desc:"Paper Toss 2.0 adds more settings to the original game. Besides the office, airport and bathroom levels we all know and love, Paper Toss 2.0 includes a cubicle, warehouse, boss and intern.",
		video:"https://www.youtube.com/embed/1wCHhQIeaiw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:73,
		category:"app",
		newApp:"new",
		
		top:"developer",
		developer:"http://fancy.com/",
		reviews:"http://www.pocket-lint.com/news/114042-fancy-app-android-review-thefancy",
		playStore:"market://details?id=com.thefancy.app",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Fancy",
		img:"http://tiptoptale.com/data/73-fancy/main.jpg",
		desc:"The Fancy app is the companion of the thefancy.com website, which is a way of sharing the exciting things you find and basically compiling a collection of things you like. Not necessarily things you'll ever buy, but things you probably would buy.",
	},
	
	{
		appid:72,
		live:"wallpaper",
		
		playStore:"market://details?id=com.kiwilwp.livewallpaper.s4nexus",
		starRating:"4.5",
		licence:"Free",
		downloads:"100,000+",
		version:"2.1",
		
		name:"Galaxy S4 Nexus Live Wallpaper",
		img:"http://tiptoptale.com/data/72-galaxy-s4-nexus-live-wallpaper/main.jpg",
		desc:"Galaxy S4 Nexus Live Wallpaper, beautiful water drop ripple effect with floating light particles. Samsung Galaxy S4 with Android 4.2.2 Jelly Bean officially unveiled at Google I/O 2013!",
	},
	
	{
		appid:71,
		category:"app",
		widget:"widget",
		
		developer:"http://www.fancywidgetsapp.com/",
		reviews:"http://www.androidtapp.com/fancy-widget-pro/",
		playStore:"market://details?id=com.anddoes.fancywidgets",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"1.6",
		
		name:"Fancy Widgets",
		img:"http://tiptoptale.com/data/71-fancy-widgets/main.jpg",
		desc:"Fancy Widget Pro is a neat set of Android home screen widgets that encompass a combination of clock, date/time, weather, and forecast features.",
	},
	
	{
		appid:70,
		category:"app",
		widget:"widget",
		
		developer:"http://www.smartandroidapps.com/",
		playStore:"market://details?id=com.smartandroidapps.equalizer",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.3",
		
		name:"Equalizer",
		img:"http://tiptoptale.com/data/70-equalizer/main.jpg",
		desc:"Improve your phone or tablet's sound quality with the first true global Equalizer app and home-screen widget! Equalizer lets you adjust sound effect levels so that you get the best out of your Music or Audio coming out of your phone.",
		video:"https://www.youtube.com/embed/WJbrXo0Bm5I?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:69,
		category:"app",
		
		top:"developer",
		developer:"http://expedia.com",
		reviews:"http://www.pcmag.com/article2/0,2817,2389230,00.asp",
		playStore:"market://details?id=com.expedia.bookings",
		starRating:"4.0",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Expedia Hotels & Flights",
		img:"http://tiptoptale.com/data/69-expedia/main.jpg",
		desc:"Much like the Expedia website (Free, 3.5 stars) it streamlines, Expedia's hotel app is friendly and modern, with subtle innovations that make it a joy to use.",
	},
	
	{
		appid:68,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"https://evernote.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2417034,00.asp",
		playStore:"market://details?id=com.evernote",
		starRating:"4.7",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"Evernote",
		img:"http://tiptoptale.com/data/68-evernote/main.jpg",
		desc:"In many ways, Evernote is similar to Microsoft OneNote (which is part of SkyDrive), although on Android devices, it's more comparable with the newly released Google Keep Android app.",
		video:"https://www.youtube.com/embed/Ag_IGEgAa9M?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:67,
		live:"wallpaper",
		
		playStore:"market://details?id=com.kiwilwp.livewallpaper.dna",
		starRating:"4.7",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Droid DNA Live Wallpaper",
		img:"http://tiptoptale.com/data/67-droid-dna-live-wallpaper/main.jpg",
		desc:"Droid DNA Live Wallpaper, inspired by HTC DNA, beautiful water drop ripple effect with floating particles.",
	},
	
	{
		appid:66,
		category:"app",
		
		developer:"http://joshclemm.com/android/earthquakealert.html",
		reviews:"http://www.cellsea.com/app/detail/android/com.joshclemm.android.quake",
		playStore:"market://details?id=com.joshclemm.android.quake",
		starRating:"4.5",
		licence:"Free",
		downloads:"500,000+",
		version:"1.6",
		
		name:"Earthquake Alert!",
		img:"http://tiptoptale.com/data/66-earthquake-alert/main.jpg",
		desc:"See the latest Magnitude 1.0 and higher earthquakes from all over the World.",
	},
	
	{
		appid:65,
		category:"game",
		
		top:"developer",
		developer:"http://www.gameloft.com/",
		reviews:"http://www.gameskinny.com/1rsxn/despicable-me-minion-rush-review-for-android",
		playStore:"market://details?id=com.gameloft.android.ANMP.GloftDMHM",
		starRating:"4.6",
		licence:"Free",
		downloads:"50,000,000+",
		version:"2.3",
		
		name:"Despicable Me",
		img:"http://tiptoptale.com/data/65-despicable-me/main.jpg",
		desc:"This game is free to play with micropayments. It's a running game much like the uber popular Temple Run game. This title was listed among one of their top free games. Besides, lets face it -- those minions are just so darn cute!",
		video:"https://www.youtube.com/embed/esimkFZ6Uys?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:64,
		category:"app",
		widget:"widget",
		
		developer:"http://www.cricbuzz.com/",
		reviews:"http://playboard.me/android/apps/com.cricbuzz.android",
		playStore:"market://details?id=com.cricbuzz.android",
		starRating:"4.7",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Cricbuzz Cricket Scores & News",
		img:"http://tiptoptale.com/data/64-cricbuzz/main.jpg",
		desc:"Live score updates and ball by ball text commentary that helps you visualize the action. Audio commentary in English and other regional languages - if listening is your thing.",
	},
	
	{
		appid:63,
		category:"app",
		
		top:"developer",
		developer:"http://path.com",
		reviews:"http://www.theverge.com/2011/11/30/2599934/path-2-0-hands-on-new-ui-sharing-options-150-friends",
		playStore:"market://details?id=com.path",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Path",
		img:"http://tiptoptale.com/data/63-path/main.jpg",
		desc:"Path is the trusted place for your personal life. It's where you can be yourself, stay close to the people who matter most, and share life instead of links. Path gives you complete control over what you share, and who you choose to share with.",
	},
	
	{
		appid:62,
		category:"game",
		
		reviews:"http://www.androidtapp.com/osmos-hd/",
		playStore:"market://details?id=com.hemispheregames.osmosdemo",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Osmos Demo",
		img:"http://tiptoptale.com/data/62-osmos-demo/main.jpg",
		desc:"Osmos HD, experience Stunning Graphics & Relaxing gaming with this must-have classic! Backed by a rich and mesmeric stereo soundtrack, it's a game that simply has to be experienced.",
		video:"https://www.youtube.com/embed/jrzhlTn1_ds?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:61,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://ubi.com/UK/",
		reviews:"http://analogaddiction.org/2013/03/04/nutty-fluffies-review/",
		playStore:"market://details?id=com.UBI.A90.WW",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"Nutty Fluffies Rollercoaster",
		img:"http://tiptoptale.com/data/61-nutty-fluffies-rollercoaster/main.jpg",
		desc:"Have you ever wanted to be a rollercoaster conductor? Now you can! Drive wild rollercoaster rides using a unique, swipe-based control system. Let the tightly tuned physics engine create the most thrilling rides of your life!",
		video:"https://www.youtube.com/embed/VMUDzXz8Axs?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:60,
		category:"app",
		widget:"widget",
		
		developer:"http://www.nq.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2404593,00.asp",
		playStore:"market://details?id=com.nqmobile.antivirus20",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"NQ Mobile Security & Antivirus",
		img:"http://tiptoptale.com/data/60-nq-mobile-security/main.jpg",
		desc:"NQ Mobile Security is an easy way to securely hide your most private photos, videos, text messages, and contacts, preventing tech-savvy snoops from accessing your most personal data.",
		video:"https://www.youtube.com/embed/sERFdQMIrQg?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:59,
		category:"app",
		
		top:"developer",
		developer:"http://www.mobisystems.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2414362,00.asp",
		playStore:"market://details?id=com.mobisystems.office",
		starRating:"4.2",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"OfficeSuite 7 (PDF & HD)",
		img:"http://tiptoptale.com/data/59-office-suite7/main.jpg",
		desc:"The latest version of OfficeSuite Pro(Free also available) brings a completely refreshed interface to an already capable mobile office suite which is well worth the price.",
	},
	
	{
		appid:58,
		live:"wallpaper",
		
		playStore:"market://details?id=com.dc.mhf",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Mystic Halo Live Wallpaper",
		img:"http://tiptoptale.com/data/58-mystic-halo-live-wallpaper/main.jpg",
		desc:"Galaxy On Fire 2 THD takes mobile gaming to a whole new level with its amazing details and rich gameplay, offering a massive galaxy for its playground.",
		video:"https://www.youtube.com/embed/QagkvXq28gM?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:57,
		category:"app",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.gotinder.com/",
		reviews:"http://www.theverge.com/2013/7/16/4529230/tinder-dating-app-now-available-android-after-huge-success-ios",
		playStore:"market://details?id=com.tinder",
		starRating:"4.2",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Tinder",
		img:"http://tiptoptale.com/data/57-tinder/main.jpg",
		desc:"Tinder is the way everyone is meeting new people. We find out who likes you nearby and connect you with them if you're also interested.",
	},
	
	{
		appid:56,
		category:"game",
		
		top:"developer",
		developer:"http://www.backflipstudios.com/",
		reviews:"http://www.androidtapp.com/ninjump/",
		playStore:"market://details?id=com.bfs.ninjump",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"NinJump",
		img:"http://tiptoptale.com/data/56-ninjump/main.jpg",
		desc:"In Ninjump the objective is to run up the walls while you fight off flying birds, squirrels and deadly ninjas chucking blades and dynamite at you.",
		video:"https://www.youtube.com/embed/4Gi5rRqzZ3M?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:55,
		category:"game",
		
		top:"developer",
		developer:"http://www.playscape.com/",
		playStore:"market://details?id=mominis.Generic_Android.Ninja_Chicken",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"Ninja Chicken",
		img:"http://tiptoptale.com/data/55-ninja-chicken/main.jpg",
		desc:"Ninja Chicken is all about a funky chicken that can jump, slide and do many stunts and wants to prove that he is indeed ninja.",
		video:"https://www.youtube.com/embed/P9Y6SRWyq1M?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:54,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://www.winamp.com/",
		reviews:"http://www.androidtapp.com/winamp/",
		playStore:"market://details?id=com.nullsoft.winamp",
		starRating:"4.3",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Winamp",
		img:"http://tiptoptale.com/data/54-winamp/main.jpg",
		desc:"Winamp is a sound music player app that performs very well. It features loads of options and tweaks and these only increase with the Pro version. If you take your music on the go very seriously, and don't mind the slightly dated appearence, Winamp is a great option.",
		video:"https://www.youtube.com/embed/Hd1Hf4D4Lrk?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:53,
		category:"game",
		newApp:"new",
		
		developer:"http://www.distinctivegames.com/",
		playStore:"market://details?id=com.distinctivegames.footballkicks",
		starRating:"4.2",
		licence:"Free",
		downloads:"10,000,000+",
		version:"1.6",
		
		name:"Football Kicks",
		img:"http://tiptoptale.com/data/53-football-kicks/main.jpg",
		desc:"Football Kicks is all about mastering your free kicks, earning your kit and locations through blood, sweat and tears on the pitch. Just swipe the screen to control the path of your shot. Practice makes perfect!",
		video:"https://www.youtube.com/embed/bIwc58Sf-4g?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:52,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://frozengun.com/",
		reviews:"www.androidtapp.com/freeze/",
		playStore:"market://details?id=com.frozengun.freeze",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Freeze!",
		img:"http://tiptoptale.com/data/52-freeze/main.jpg",
		desc:"Freeze! is impeccably designed and the graphics are incredibly rich and detailed. While the colour-scheme is decidedly monochrome, the animations are wonderfully detailed and look fantastic as you play.",
		video:"https://www.youtube.com/embed/rNFvnIYi1DQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:51,
		category:"game",
		newApp:"new",
		
		developer:"https://support.fishlabs.net/",
		reviews:"http://www.androidpolice.com/2011/06/16/galaxy-on-fire-2-thd-lands-in-the-android-market-and-its-free-somewhat/",
		playStore:"market://details?id=net.fishlabs.gof2hdallandroid2012",
		starRating:"4.4",
		licence:"Free",
		downloads:"100,000+",
		version:"3.1",
		
		name:"Galaxy on Fire 2&trade; HD",
		img:"http://tiptoptale.com/data/51-galaxy-on-fire2/main.jpg",
		desc:"Galaxy On Fire 2 THD takes mobile gaming to a whole new level with its amazing details and rich gameplay, offering a massive galaxy for its playground.",
		video:"https://www.youtube.com/embed/2lFizVHU8DU?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:50,
		category:"app",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.fxguruapp.com/",
		reviews:"http://www.appszoom.com/android_applications/entertainment/fxguru-movie-fx-director_fbvnl.html",
		playStore:"market://details?id=com.picadelic.fxguru",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"FxGuru: Movie FX Director",
		img:"http://tiptoptale.com/data/50-fxguru/main.jpg",
		desc:"We know that video editing is one the unresolved matters for Android. There are just several apps able to cut and paste and merge clips on the go, but there are even less apps able to add special effects to your videos beyond hipster Instagram-like filters.",
		video:"https://www.youtube.com/embed/Tztev0Q-CN8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:49,
		category:"app",
		
		reviews:"http://download.cnet.com/Gallery-Lock/3000-2094_4-75454003.html",
		playStore:"market://details?id=com.morrison.gallerylocklite",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Gallery Lock (Hide pictures)",
		img:"http://tiptoptale.com/data/49-gallery-lock/main.jpg",
		desc:"The security of one's media often happens to be an overlooked subject. Gallery Lock provides that extra layer of security. While useful to some, Gallery Lock tends to save its premium features for those willing to pay for the pro version.",
		video:"https://www.youtube.com/embed/YPHDcnwUC6E?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:48,
		live:"wallpaper",
		newApp:"new",
		
		playStore:"market://details?id=com.abusivestudios.gateoftime",
		starRating:"4.8",
		licence:"Free",
		downloads:"100,000+",
		version:"2.1",
		
		name:"Gate of Time Live Wallpaper",
		img:"http://tiptoptale.com/data/48-gate-of-time-live-wallpaper/main.jpg",
		desc:"The Gate of Time as inspired by Nintendo's The Legend of Zelda: Skyward Sword! Screenshots don't do it justice at all. The effect is entirely in the motion. NO Airpush. NO Admob. NO ads of any kind.",
		video:"https://www.youtube.com/embed/23bvEHABe6Y?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:47,
		category:"game",
		
		top:"developer",
		developer:"http://halfbrick.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2379811,00.asp",
		playStore:"market://details?id=com.halfbrick.fruitninjafree",
		starRating:"4.5",
		licence:"Free",
		downloads:"100,000,000+",
		version:"2.3",
		
		name:"Fruit Ninja Free",
		img:"http://tiptoptale.com/data/47-fruit-ninja-free/main.jpg",
		desc:"Fruit Ninja is a juicy action game with squishy, splatty and satisfying fruit carnage! Become the ultimate bringer of sweet, tasty destruction with every single slash!",
		video:"https://www.youtube.com/embed/nCo6OLNkxgo?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:46,
		newApp:"new",
		live:"wallpaper",
		
		playStore:"market://details?id=com.kiwilwp.livewallpaper.galaxys4",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.1",
		
		name:"Galaxy S4 Live Wallpaper",
		img:"http://tiptoptale.com/data/46-galaxy-s4-live-wallpaper/main.jpg",
		desc:"Galaxy S4 Live Wallpaper, beautiful water drop ripple effect with floating light particles. Now with dragging light particles option that is similar to original Samsung Galaxy S4 lock screen effect!",
	},
	
	{
		appid:45,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.glu.com/",
		reviews:"http://www.modojo.com/reviews/gears__guts",
		playStore:"market://details?id=com.glu.carszombies",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Gears & Guts",
		img:"http://tiptoptale.com/data/45-gears-and-guts/main.jpg",
		desc:"Grab your keys. Fasten your seatbelt. It's time to put some blood on the highway. Crush the undead beneath your tires. Tear the undead limb-from-rotten-limb with whatever military gear and experimental weapons you can bolt to your car.",
		video:"https://www.youtube.com/embed/hbjeGTuUt-0?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:44,
		category:"game",
		
		developer:"http://www.dedalord.com/",
		playStore:"market://details?id=com.dedalord.fallingfred",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"Falling Fred",
		img:"http://tiptoptale.com/data/44-falling-fred/main.jpg",
		desc:"Lead Fred in his futile exercise to avoid the inevitable - becoming a human slice of minced meat. Dodge obstacles, avoid limb damage when possible but remember to keep your head attached to your shoulders, or it's the end of the trip for you.",
		video:"https://www.youtube.com/embed/NBu6hJv5hJc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:43,
		category:"game",
		
		developer:"http://chaos-game.com/en/",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/chaos-tournament_dffyt.html",
		playStore:"market://details?id=com.skyjet.chaosarena",
		starRating:"4.7",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"C.H.A.O.S Tournament",
		img:"http://tiptoptale.com/data/43-chaos-tournament/main.jpg",
		desc:"An attack helicopter combat simulator; OK, hold tight to your couch, because CHAOS isn't a casual game. We repeat, this is not a simulacrum, CHAOS Tournament isn't a casual game.",
		video:"https://www.youtube.com/embed/0NLW-MI0-yA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:42,
		category:"game",
		multi:"multi",
		
		top:"developer",
		developer:"http://www.gameloft.com/?sk",
		reviews:"http://www.appszoom.com/android_games/arcade_and_action/brothers-in-arms-2-free_bsnol.html",
		playStore:"market://details?id=com.gameloft.android.ANMP.GloftB2HM",
		starRating:"4.3",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Brothers In Arms 2",
		img:"http://tiptoptale.com/data/42-brothers-in-arms2/main.jpg",
		desc:"The game is loaded with excellent graphics, multiplayer, and a price of $0. There is a slight catch to it being free, however; in addition to having ads, the game offers the ability to purchase Medals to unlock new features and gear more quickly, but this isn't necessary.",
		video:"https://www.youtube.com/embed/lj3Y6guWh9A?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:41,
		category:"game",
		multi:"multi",
		newApp:"new",
		
		developer:"http://poker.boyaagame.com/android.html",
		reviews:"http://www.appszoom.com/android_games/cards_and_casino/boyaa-texas-poker_cggfw.html",
		playStore:"market://details?id=com.boyaa.texaspokerin",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000+",
		version:"2.0",
		
		name:"Boyaa Texas Poker",
		img:"http://tiptoptale.com/data/41-boyaa-texas-poker/main.jpg",
		desc:"Boyaa Poker is a good way to start playing poker online for two reasons: it's free (unless you want to use in-app purchase to buy extra chips) and there's a step-by-step tutorial that will help you to learn basic Hold'em rules.",
	},
	
	{
		appid:40,
		category:"game",
		
		developer:"http://www.runnergameshk.com/",
		reviews:"http://www.appszoom.com/android_games/racing/bmx-boy_cfyxy.html",
		playStore:"market://details?id=com.game.BMX_Boy",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"BMX Boy",
		img:"http://tiptoptale.com/data/40-bmx-boy/main.jpg",
		desc:"BMX Boy is another great title developed by RUNNERGAMES. Actually it's quite similar to its predecessor 'Skate Boy'. The main difference is that stages and tricks are different. And of course, here the kid is riding a bike.",
		video:"https://www.youtube.com/embed/lvhFBctIgLw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:39,
		category:"game",
		multi:"multi",
		
		reviews:"http://blobby-volley.en.softonic.com/",
		playStore:"market://details?id=com.appson.blobbyvolley",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Blobby Volleyball",
		img:"http://tiptoptale.com/data/39-blobby-volleyball/main.jpg",
		desc:"Blobby Volley plays surprisingly well and the lack of tanned or bronzed bodies jumping around isn't missed one bit. The graphics are not exactly stunning - the volleyball pitch is static and the blobs are, well, blobs but it's the simple playability that appeals.",
	},
	
	{
		appid:38,
		category:"game",
		multi:"multi",
		
		developer:"http://www.topfreegames.com",
		reviews:"http://bike-race-free.en.softonic.com/android",
		playStore:"market://details?id=com.topfreegames.bikeracefreeworld",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Bike Race Free",
		img:"http://tiptoptale.com/data/38-bike-race-free/main.jpg",
		desc:"Bike Race Free is a simple, but challenging arcade puzzle game that uses the accelerometer of device to control the tilt of the bike.",
	},
	
	{
		appid:37,
		category:"game",
		newApp:"new",
		
		developer:"http://www.nextwavemultimedia.com/",
		playStore:"market://details?id=com.nextwave.BeachCricketFree",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.0",
		
		name:"Beach Cricket",
		img:"http://tiptoptale.com/data/37-beach-cricket/main.jpg",
		desc:"For the cricket enthusiast, the game can be adapted to suit any surrounding. In this version have a go at 'Beach Cricket' and get a taste of the sun, sand and the sea while the willow smacks the cherry!",
	},
	
	{
		appid:36,
		category:"game",
		
		top:"developer",
		developer:"http://www.game-insight.com/support/android",
		reviews:"http://www.appbrain.com/app/battle-towers/com.gameinsight.battletowersandroid",
		playStore:"market://details?id=com.gameinsight.battletowersandroid",
		starRating:"4.5",
		licence:"Free",
		downloads:"100,000+",
		version:"2.2",
		
		name:"Battle Towers",
		img:"http://tiptoptale.com/data/36-battle-towers/main.jpg",
		desc:"Battle Dragons is a canny blend of tycoon and tower defense game, where you raise and manage a band of dragons to defend your base and attack others.",
	},
	
	{
		appid:35,
		category:"game",
		newApp:"new",
		
		developer:"http://nextwavemultimedia.com",
		playStore:"market://details?id=com.nextwave.battleofchepauk",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000+",
		version:"2.0",
		
		name:"Battle Of Chepauk",
		img:"http://tiptoptale.com/data/35-battle-of-chepauk/main.jpg",
		desc:"Chennai Super Kings Battle of Chepauk is a fun to play cricket game offering you hours of entertainment. You own one of the best T20 teams.",
		video:"https://www.youtube.com/embed/hHKkQIk5ljw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:34,
		category:"game",
		multi:"multi",
		
		developer:"http://www.geekbeach.com/",
		reviews:"http://www.androidpolice.com/2012/07/09/battle-monkeys-a-game-with-apes-guns-explosions-and-lava-need-i-say-more/",
		playStore:"market://details?id=com.geekbeach.battlemonkeys",
		starRating:"4.6",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"Battle Monkeys Multiplayer",
		img:"http://tiptoptale.com/data/34-battle-monkeys-multiplayer/main.jpg",
		desc:"Battle Monkeys: A Game With Apes, Guns, Explosions, And Lava - Need I Say More? Why aren't you downloading this already? Oh, you want some explanation? Okay check out the reviews.",
		video:"https://www.youtube.com/embed/5m_0kOkyv0M?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:33,
		category:"game",
		
		reviews:"http://basketball-shoot.m.en.softonic.com/android",
		playStore:"market://details?id=com.game.basketballshoot",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Basketball Shoot",
		img:"http://tiptoptale.com/data/33-basketball-shoot/main.jpg",
		desc:"Basketball Shoot is an simple but very addictive game which base on realistic physics. The quantity of the basketballs is limited, your goal is to shoot them for a better score.",
	},
	
	{
		appid:32,
		category:"app",
		
		developer:"http://code.google.com/p/zxing/wiki/FrequentlyAskedQuestions",
		reviews:"http://www.trustedreviews.com/opinions/10-of-the-best-android-apps_Page-2",
		playStore:"market://details?id=com.google.zxing.client.android",
		starRating:"4.2",
		licence:"Free",
		downloads:"50,000,000+",
		
		name:"Barcode Scanner",
		img:"http://tiptoptale.com/data/32-barcode-scanner/main.jpg",
		desc:"Scan barcodes on products then look up prices and reviews. You can also scan Data Matrix and QR Codes containing URLs, contact info, etc. Also share your contacts, apps, and bookmarks via QR Code.",
	},
	
	{
		appid:31,
		category:"app",
		
		developer:"https://www.facebook.com/backgroundshd",
		reviews:"http://www.appszoom.com/android_themes/wallpapers/backgrounds-hd-wallpapers_enw.html",
		playStore:"market://details?id=com.ogqcorp.bgh",
		starRating:"4.8",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.1",
		
		name:"Backgrounds HD",
		img:"http://tiptoptale.com/data/31-backgrounds-and-wallpapersHD/main.jpg",
		desc:"This is one of those things that do not lie to anyone. They promised 10000 wallpapers so you already knew what you get out there. There are dozens of wallpaper apps. But if I had to choose just one, it would be this.",
		video:"https://www.youtube.com/embed/N4D6DDIf7RU?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:30,
		category:"game",
		
		top:"developer",
		developer:"http://www.ama-studios.com/",
		reviews:"http://www.androidpolice.com/2012/06/15/game-review-babel-rising-3d-is-a-rich-well-made-time-waster-but-is-it-worth-5/",
		playStore:"market://details?id=com.mando.babelrising3dfreemium&hl=en",
		starRating:"4.2",
		licence:"Free",
		downloads:"5,000+",
		version:"2.2",
		
		name:"Babel Rising 3D",
		img:"http://tiptoptale.com/data/30-babel-rising/main.jpg",
		desc:"The premise of Babel Rising 3D is that you're an angry god trying to smite his people for erecting a tower in honour of some heathen deity. You strike down these lemming-sized heretics with a variety of elements - earth, fire, wind, and water (no heart, sorry Ma-Ti).",
		video:"https://www.youtube.com/embed/_OHjHVAGn9I?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:29,
		category:"app",
		widget:"widget",
		newApp:"new",
		
		developer:"http://www.speaktoit.com",
		reviews:"http://www.tomsguide.com/us/Speaktoit-Assistant-Siri-Yelp-Facebook,news-16666.html#Speaktoit-Assistant-Siri-Yelp-Facebook%2Cnews-16666.html?&_suid=138220016187505379094964036093",
		playStore:"market://details?id=com.speaktoit.assistant",
		starRating:"4.7",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.1",
		
		name:"Assistant",
		img:"http://tiptoptale.com/data/29-assistant/main.jpg",
		desc:"To be honest, the Speaktoit Assistant is a bit more personal that Apple's stiff-voiced Siri. It seems more comfortable in having conversations with the device owner, using a rather fluid voice when compared to Siri's robotic pronunciations.",
		video:"https://www.youtube.com/embed/nulMVeyqupQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:28,
		category:"app",
		
		developer:"http://www.agilesoftresource.com/",
		reviews:"http://www.appszoom.com/android_applications/productivity/androzip-file-manager_bve.html",
		playStore:"market://details?id=com.agilesoftresource&hl=en",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"AndroZip&trade; File Manager",
		img:"http://tiptoptale.com/data/28-androZip/main.jpg",
		desc:"AndroZip File Manager is an all-in-one file manager and task killer. AndroZip lets you zip and unzip .zip, .rar, .7zip (.Gzip, .tar, .bzip2 are in beta but you can even create that format files).",
	},
	
	{
		appid:27,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://www.any.do/",
		reviews:"http://www.pcmag.com/article2/0,2817,2420108,00.asp",
		playStore:"market://details?id=com.anydo&hl=en",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"Any.do To-do List & Task List",
		img:"http://tiptoptale.com/data/27-any.do/main.jpg",
		desc:"Simple, synchronized to-do list app. Excellent and unique feature, Any.do moment, encourages the habit of daily to-do review. Can view tasks by date or category. Free.",
	},
	
	{
		appid:26,
		multi:"multi",
		category:"game",
		
		top:"developer",
		developer:"http://www.gameloft.com/android-games/asphalt-7/?adid=101966",
		reviews:"http://www.pcmag.com/article2/0,2817,2411189,00.asp",
		playStore:"market://details?id=com.gameloft.android.ANMP.GloftA7HM",
		starRating:"4.2",
		licence:"$0.99",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"Asphalt 7: Heat",
		img:"http://tiptoptale.com/data/26-asphalt-7-heat/main.jpg",
		desc:"Asphalt 7: Heat isn't a simulator, but it's definitely a great buy for old-school Ridge Racer and Outrun fans, or anyone who wants a real arcade-style racing game-without excuses.",
		video:"https://www.youtube.com/embed/5wK3RRkecPA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:25,
		category:"app",
		
		reviews:"http://www.appbrain.com/app/webmasters-html-editor-lite/com.welant.webmaster.DEMO",
		playStore:"market://details?id=com.webeditlite.app&hl=en",
		starRating:"4.1",
		licence:"Free",
		downloads:"100,000+",
		version:"1.6",
		
		name:"Android Web Editor Lite",
		img:"http://tiptoptale.com/data/25-android-web-editor-lite/main.jpg",
		desc:"Android Web Editor is a must-have application for web developers. If you need to test a snippet of PHP or edit a page on a client's website while you're on the go, you don't need to fire up your laptop if you have an Android mobile device.",
		video:"https://www.youtube.com/embed/RiLt4V5DNdw?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:24,
		category:"app",
		
		developer:"http://www.lysesoft.com/",
		reviews:"http://www.appszoom.com/android_applications/tools/andftp-your-ftp-client_xwl.html",
		playStore:"market://details?id=lysesoft.andftp&hl=en",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"1.6",
		
		name:"AndFTP (your FTP client)",
		img:"http://tiptoptale.com/data/24-andFTP/main.jpg",
		desc:"AndFTP is a FTP, FTPS, SCP and SFTP client with lots of features. You can add and manage several FTP configurations. If you're looking for a reliable FTP client you've just found one.",
	},
	
	{
		appid:23,
		category:"app",
		
		developer:"http://support.mantano.com/",
		playStore:"market://details?id=com.mantano.reader.android.lite",
		starRating:"4.2",
		licence:"Free",
		downloads:"500,000+",
		version:"2.3",
		
		name:"Mantano Ebook Reader Free",
		img:"http://tiptoptale.com/data/23-mantano-ebook/main.jpg",
		desc:"The free edition (with ads) of the best ebook reader on the market: user friendly, powerful, fast, this book reader provides unprecedented speed and reading comfort. Download thousands of free and paid ebooks, highlight excerpts, take notes in your books and contracts, organize your books, add your favorite catalogs in your 'Bookstores' area and much more!",
	},
	
	{
		appid:22,
		category:"game",
		multi:"multi",
		
		developer:"http://www.full-fat.com/support/",
		reviews:"http://www.androidcentral.com/agent-dash-review-spy-has-infiltrated-temple-run",
		playStore:"market://details?id=com.fullfat.android.agentdash",
		starRating:"4.5",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.3",
		
		name:"Agent Dash",
		img:"http://tiptoptale.com/data/22-agent-dash/main.jpg",
		desc:"Agent Dash - a spy has infiltrated Temple Run. Despite having a gameplay mechanic very similar to Temple Run, the graphics and animation are leagues ahead.",
	},
	
	{
		appid:21,
		category:"game",
		newApp:"new",
		
		top:"developer",
		developer:"http://www.game-insight.com/",
		reviews:"http://www.gamezebo.com/games/2020-my-country/review",
		playStore:"market://details?id=com.gameinsight.mycountry2020",
		starRating:"4.5",
		licence:"Free",
		downloads:"500,000+",
		version:"2.1",
		
		name:"2020: My Country",
		img:"http://tiptoptale.com/data/21-2020-my-country/main.jpg",
		desc:"2020: My Country offers gorgeous graphics with highly detailed animations that make your city, and the hundreds of quests you conquer, come to life.",
		video:"https://www.youtube.com/embed/o3dRVuNyuIs?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:20,
		category:"app",
		newApp:"new",
		
		developer:"http://www.4shared.com/",
		reviews:"http://4shared-mobile.en.softonic.com/android",
		playStore:"market://details?id=com.forshared",
		starRating:"4.4",
		licence:"Free",
		downloads:"10,000,000+",
		
		name:"4shared",
		img:"http://tiptoptale.com/data/20-4shared/main.jpg",
		desc:"Share photos, music, videos and other files with 4shared Mobile for Android. Setting up an account comes with 10GB of free storage space, and this gives you access to your web-based 4Sync account.",
	},
	
	{
		appid:19,
		category:"app",
		
		developer:"https://sites.google.com/site/mxvpen/",
		reviews:"http://www.pocket-lint.com/news/115379-mx-player-android-app-review",
		playStore:"market://details?id=com.mxtech.videoplayer.ad&hl=en",
		starRating:"4.7",
		licence:"Free",
		downloads:"50,000,000+",
		version:"2.1",
		
		name:"MX Player",
		img:"http://tiptoptale.com/data/19-mx-player/main.jpg",
		desc:"While there are quite a few video players on the Google Play, it's hard to pick the one that has a good range of format support, while still being both free and easy to use. MX Player couldn't make less of a fuss about itself if it tried.",
	},
	
	{
		appid:18,
		category:"app",
		widget:"widget",
		newApp:"new",
		
		developer:"http://www.grinzone-apps.com/incall-recorder.html",
		reviews:"http://www.appbrain.com/app/mp3-incall-recorder-voice/com.grinzone.incallrecorder",
		playStore:"market://details?id=com.grinzone.incallrecorder&hl=en",
		starRating:"4.4",
		licence:"Free",
		downloads:"100,000+",
		version:"2.3",
		
		name:"MP3 InCall Recorder & Voice (Removed)",
		img:"http://tiptoptale.com/data/18-mp3-incall-recorder-and-voice/main.jpg",
		desc:"High Quality MP3 Calls & Voice recorder in one. You can choose where to place the recording start button. You can play, share, or add a note to each call recorded.",
		video:"https://www.youtube.com/embed/Nm2yTuXesic?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:17,
		category:"app",
		
		developer:"http://hideitpro.com/",
		reviews:"http://www.appbrain.com/app/hide-pictures-hide-it-pro/com.smartanuj.hideitpro",
		playStore:"market://details?id=com.smartanuj.hideitpro&hl=en",
		starRating:"4.8",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.0",
		
		name:"Hide Pictures - Hide It Pro (Audio Manager)",
		img:"http://tiptoptale.com/data/17-hide-it-pro/main.jpg",
		desc:"Hide Pictures, Videos, Applications, Messages, Calls in your phone. COMPLETELY FREE and UNLIMITED version. The app is cleverly disguised as Audio Manager in the App Drawer.",
		video:"https://www.youtube.com/embed/XW2wLkEGxg8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:16,
		category:"game",
		
		top:"developer",
		developer:"http://www.glu.com/",
		reviews:"http://www.gamezebo.com/games/frontline-commando-d-day/review",
		playStore:"market://details?id=com.glu.flcn_new",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.1",
		
		name:"Frontline Commando: D-Day",
		img:"http://tiptoptale.com/data/16-frontline-commando-d-day/main.jpg",
		desc:"Experience the ultimate 3rd person shooter with stunning console quality visuals, precise controls, advanced physics, destructible environments and full voiceovers. Jump into the heat of battle and push your device to the limit!",
		video:"https://www.youtube.com/embed/G6ObyCEt6_k?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:15,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"http://www.flipboard.com/",
		reviews:"http://www.engadget.com/2013/05/09/flipboard-brings-magazine-curation-to-android/",
		playStore:"market://details?id=flipboard.app",
		starRating:"4.5",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Flipboard: Your News Magazine",		
		img:"http://tiptoptale.com/data/15-flipboard/main.jpg",
		desc:"Flipboard is your personal magazine, filled with the things you care about. Catch up on the news, discover amazing things from around the world, or stay connected to the people closest to you-all in one place.",
	},
	
	{
		appid:14,
		category:"app",
		widget:"widget",
		
		top:"developer",
		developer:"https://www.dropbox.com/home",
		reviews:"http://www.pcmag.com/article2/0,2817,2399830,00.asp",
		playStore:"market://details?id=com.dropbox.android&hl=en",
		starRating:"4.6",
		licence:"Free",
		downloads:"100,000,000+",
		version:"2.2",
		
		name:"Dropbox",		
		img:"http://tiptoptale.com/data/14-dropbox/main.jpg",
		desc:"Dropbox is a free service that lets you bring all your photos, docs, and videos anywhere. Save photos or videos to your Dropbox and share them with friends in just a couple taps.",
		video:"https://www.youtube.com/embed/w4eTR7tci6A?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:13,
		category:"game",
		
		developer:"http://dfcgames.com/",
		playStore:"market://details?id=com.dfcgames.CrashCourseICE&hl=en",
		starRating:"4.5",
		licence:"Free",
		downloads:"50,000+",
		version:"2.2",
		
		name:"Crash Course 3D: ICE",		
		img:"http://tiptoptale.com/data/13-crash-course-3d-ice/main.jpg",
		desc:"Crash Course 3D is a fast-pace arcade space shooter that used 3D technology. Gameplay is similar to other classic space shooters like Asteroids: a massive armada of aliens are invading human bases on unprotected planets.",
		video:"https://www.youtube.com/embed/-QXRIJH6Gx8?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:12,
		category:"game",
		
		top:"developer",
		developer:"http://www.glu.com/",
		reviews:"http://www.supergamedroid.com/2013/07/12/contract-killer-zombies-2-origins-review/",
		playStore:"market://details?id=com.glu.ckzombies2&hl=en",
		starRating:"4.7",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.1",
		
		name:"CKZ ORIGINS",		
		img:"http://tiptoptale.com/data/12-ckz-origins/main.jpg",
		desc:"The game is based around arena-like stages, similar to other games like Dead Trigger. It offers some really cool things and some arguably bad ones, I personally consider it a good game.",
		video:"https://www.youtube.com/embed/sEE2dugZYxA?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:11,
		category:"app",
		
		top:"developer",
		developer:"https://www.google.com/intl/en/chrome/browser/mobile/android.html",
		reviews:"http://www.pcmag.com/article2/0,2817,2406535,00.asp",
		playStore:"market://details?id=com.android.chrome&hl=en",
		starRating:"4.2",
		licence:"Free",
		downloads:"50,000,000+",
		version:"4.0",
		
		name:"Chrome Browser",		
		img:"http://tiptoptale.com/data/11-chrome-browser/main.jpg",
		desc:"Browse fast with the Chrome web browser on your Android phone and tablet. Sign in to sync your Chrome browser experience from your computer to bring it with you anywhere you go.",
		video:"https://www.youtube.com/embed/lVjw7n_U37A?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:10,
		category:"game",
		newApp:"new",
		multi:"multi",
		
		top:"developer",
		developer:"http://about.king.com/candy-crush-saga-faqs",
		reviews:"http://www.pcmag.com/article2/0,2817,2421560,00.asp",
		playStore:"market://details?id=com.king.candycrushsaga&hl=en",
		starRating:"4.4",
		licence:"Free",
		downloads:"50,000,000+",
		version:"2.2",
		
		name:"Candy Crush Saga",		
		img:"http://tiptoptale.com/data/10-candy-crush-saga/main.jpg",
		desc:"Candy Crush Saga is among that class of mobile games that defines the casual genre. Easy to play and even easier to get addicted to, Candy Crush Saga could easily suck up all your time if you let it.",
		video:"https://www.youtube.com/embed/CWQHEqS9-vQ?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:9,
		category:"app",
		
		top:"developer",
		developer:"https://www.camscanner.net/",
		reviews:"http://www.appbrain.com/app/camscanner-phone-pdf-creator/com.intsig.camscanner",
		playStore:"market://details?id=com.intsig.camscanner&hl=en",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.0",
		
		name:"CamScanner - Phone PDF Creator",		
		img:"http://tiptoptale.com/data/9-cam-scanner-phone-pdf/main.jpg",
		desc:"Turn any Smartphone into a Scanner with CamScanner for Intelligent Document Management. It is the perfect fit for those who want to scan, sync, edit, share and manage various contents on all devices.",
		video:"https://www.youtube.com/embed/0E9X_x4AJHc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:8,
		category:"app",
		widget:"widget",
		newApp:"new",
		
		developer:"http://www.avgmobilation.com/",
		reviews:"http://download.cnet.com/AVG-Memory-Cache-Cleaner/3000-2094_4-75911909.html",
		playStore:"market://details?id=com.avg.cleaner",
		starRating:"4.5",
		licence:"Free",
		downloads:"100,000+",
		version:"2.1",
		
		name:"AVG Memory & Cache Cleaner",		
		img:"http://tiptoptale.com/data/8-avg-memory-and-cache-cleaner/main.jpg",
		desc:"AVG Cleaner lets you quickly erase and clear your browser, call and text histories, as well as identify and remove unwanted cached app data from both the device's internal memory and the SD card.",
	},
	
	{
		appid:7,
		category:"app",
		widget:"widget",
		
		developer:"http://www.domobile.com/",
		reviews:"http://www.pcmag.com/article2/0,2817,2422517,00.asp",
		playStore:"market://details?id=com.domobile.applock",
		starRating:"4.6",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"AppLock",		
		img:"http://tiptoptale.com/data/7-app-lock/main.jpg",
		desc:"AppLock can lock SMS, Contacts, Gmail, Facebook, Gallery, Market, Settings, Calls and any app you choose, with abundant options, protecting your privacy",
		video:"https://www.youtube.com/embed/tVyzDUs59iI?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:6,
		category:"app",
		widget:"widget",
		
		developer:"http://www.avgmobilation.com/",
		reviews:"http://www.androidpolice.com/2011/08/30/mobile-security-app-shootout-part-10-anti-virus-from-avg-mobilation-can-handle-loss-theft-and-malware-with-ease/",
		playStore:"market://details?id=com.antivirus&hl=en",
		starRating:"4.6",
		licence:"Free",
		downloads:"50,000,000+",
		
		name:"AntiVirus Security - FREE",		
		img:"http://tiptoptale.com/data/6-antivirus-security-free/main.jpg",
		desc:"AVG AntiVirus FREE for Android protects you from harmful viruses, malware, spyware and text messages and helps keep your personal data safe. Download Free Now!",		
	},
	
	{
		appid:5,
		category:"app",
		
		developer:"http://www.airdroid.com/",
		reviews:"http://www.androidpolice.com/2013/04/25/exclusive-first-look-airdroid-version-2-headed-for-the-play-store-today-heres-what-youll-find-inside/",
		playStore:"market://details?id=com.sand.airdroid&hl=en",
		starRating:"4.7",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.1",
		
		name:"AirDroid",		
		img:"http://tiptoptale.com/data/5-airdroid/main.jpg",
		desc:"AirDroid gives you the ability to access all of the files on your phone - APKs, music, photos, videos and plenty more - directly in a web app on your browser, which offers an Android-like user interface.",
		video:"https://www.youtube.com/embed/g_vOc-9SROE?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:4,
		category:"game",
		newApp:"new",
		multi:"multi",
		
		top:"developer",
		developer:"http://www.miniclip.com",
		reviews:"http://www.appbrain.com/app/8-ball-pool/com.miniclip.eightballpool",
		playStore:"market://details?id=com.miniclip.eightballpool&hl=en",
		starRating:"4.6",
		licence:"Free",
		downloads:"5,000,000+",
		version:"2.2",
		
		name:"8 Ball Pool",		
		img:"http://tiptoptale.com/data/4-8-ball-pool/main.jpg",
		desc:"Play with friends! Play with Legends. Play the hit Miniclip Pool game on your mobile. Play with friends then compete 1 on 1 as you strive to become a Pool God!",		
	},
	
	{
		appid:3,
		category:"app",
		
		top:"developer",
		developer:"http://www.swiftkey.net/en/",
		reviews:"http://www.theverge.com/2013/2/20/4003960/swiftkey-flow-swype-gesture-typing-android-keyboard-released",
		playStore:"market://details?id=com.touchtype.swiftkey.phone.trial",
		starRating:"4.5",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.2",
		
		name:"SwiftKey Keyboard Free",		
		img:"http://tiptoptale.com/data/3-swiftKey-keyboard-free/main.jpg",
		desc:"SwiftKey replaces your phone's keyboard with one that understands you. SwiftKey learns as you type, adapting to you and your way of writing. Switch between tapping and gesture-typing with SwiftKey Flow.",
		video:"https://www.youtube.com/embed/f9Ukb6Migl0?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:2,
		category:"app",
		
		top:"developer",
		developer:"http://www.magisto.com/",
		reviews:"http://www.engadget.com/2013/01/07/magisto-sharpens-its-ai-video-editing-algorithm-adds-themes/",
		playStore:"market://details?id=com.magisto",
		starRating:"4.4",
		licence:"Free",
		downloads:"1,000,000+",
		version:"2.3",
		
		name:"Magisto Video Editor & Sharing",		
		img:"http://tiptoptale.com/data/2-magisto-video-editor-and-sharing/main.jpg",
		desc:"Magisto Magical Video Editor + Photos automatically turns your video clips and pictures into beautifully edited movies, complete with music and effects, in minutes.",
		video:"https://www.youtube.com/embed/Rb1awERp92E?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},
	
	{
		appid:1,
		category:"game",
		
		top:"developer",
		developer:"http://www.zeptolab.com/",
		reviews:"http://www.trustedreviews.com/cut-the-rope-time-travel_Mobile-App_review",
		playStore:"market://details?id=com.zeptolab.timetravel.free.google&hl=en",
		starRating:"4.8",
		licence:"Free",
		downloads:"10,000,000+",
		version:"2.2",
		
		name:"Cut the Rope: Time Travel",		
		img:"http://tiptoptale.com/data/1-cut-the-rope-time-travel/main.jpg",
		desc:"Cut the Rope: Time Travel is twice the fun but familiar to play. If you like Cut the Rope, you'll love Cut the Rope: Time Travel!",
		video:"https://www.youtube.com/embed/ALj3Aqzhzhc?ps=play&vq=large&rel=0&autohide=1&showinfo=0&autoplay=1",
	},

];